var sqlite3 = require('sqlite3');
var db = new sqlite3.Database("maindb.sqlite3");

db.serialize(function() {
	db.run(`CREATE TABLE players (
		discordid TEXT UNIQUE PRIMARY KEY,
		krunker TEXT UNIQUE,
		nickname TEXT NOT NULL,
		country TEXT,
		premium INTEGER DEFAULT 0 NOT NULL,
		punished INTEGER DEFAULT 0 NOT NULL,
		class INTEGER DEFAULT 0 NOT NULL,
		team TEXT
		)`);
	db.run(`CREATE TABLE stats (
		nextmatchid INTEGER NOT NULL,
		nextplayerid INTEGER NOT NULL,
		nextleagueid INTEGER NOT NULL,
		nextpickupid INTERER NOT NULL
		)`);
	db.run(`INSERT INTO stats (nextmatchid, nextplayerid, nextleagueid, nextpickupid)
		VALUES (1,1,1,1)`);
	db.run(`CREATE TABLE guilds (
		guildid TEXT UNIQUE PRIMARY KEY,
		leagueid INTEGER NOT NULL,
		overlordid TEXT NOT NULL,
		name TEXT UNIQUE,

		mainpickupid INTEGER,
		adminrole TEXT,
		modrole TEXT,
		readytime INTEGER DEFAULT 30000,
		noaddrole TEXT,
		channelids TEXT,
		matchchannel TEXT,
		promotedelay INTEGER DEFAULT 1800000,
		promoterole TEXT,
		placementgames INTEGER DEFAULT 10
		)`);
	db.run(`CREATE TABLE pickups (
		name TEXT,
		pickupid INTEGER UNIQUE PRIMARY KEY,
		guildid TEXT,
		leagueid INTEGER,

		whiterole TEXT,
		blackrole TEXT,
		mappool TEXT,
		pickteams TEXT DEFAULT "auto",
		ranked INTEGER DEFAULT 0,
		teamsize INTEGER DEFAULT 4,
		placementgames INTEGER DEFAULT 10
		)`);
	db.run(`CREATE TABLE infractions (
		discordid TEXT NOT NULL,
		guildid TEXT NOT NULL,
		issuetime INTEGER NOT NULL,
		duration INTEGER NOT NULL,
		reason TEXT,
		issuer TEXT NOT NULL,
		forgiven TEXT
		)`);
	db.run(`CREATE TABLE matches (
		matchid INTEGER UNIQUE PRIMARY KEY,
		pickupid INTEGER NOT NULL,
		team1list TEXT,
		team2list TEXT,
		result TEXT,
		starttime INTEGER,
		map TEXT,
		team1sum REAL,
		team2sum REAL
		)`);
	db.run(`CREATE TABLE elochanges (
		matchid INTEGER NOT NULL,
		discordid TEXT NOT NULL,
		leagueid INTEGER NOT NULL,
		muchange REAL,
		sigchange REAL,
		classes INTEGER,
		PRIMARY KEY (matchid, discordid, leagueid)
		)`);
	db.run(`CREATE TABLE ranks (
		discordid TEXT NOT NULL,
		leagueid INTEGER NOT NULL,
		mu REAL,
		sig REAL,
		maxmu REAL DEFAULT -50,
		maxsig REAL DEFAULT 8.33333,
		wins INTEGER DEFAULT 0,
		losses INTEGER DEFAULT 0,
		draws INTEGER DEFAULT 0,
		streak INTEGER DEFAULT 0,
		PRIMARY KEY (discordid, leagueid)
		)`);
	db.run(`CREATE TABLE leagues (
		leagueid INTEGER UNIQUE PRIMARY KEY,
		name TEXT,
		placementgames INTEGER DEFAULT 10,
		starttime INTEGER,
		locktime INTEGER,
		guildid TEXT,
		pickupid INTEGER,
		hidden INTEGER DEFAULT 0,

		s_mostwonplayer REAL,
		s_mostwonplayerinfo TEXT,
		s_mostwonteam REAL,
		s_mostwonteaminfo TEXT,
		s_mostlostplayer REAL,
		s_mostlostplayerinfo TEXT,
		s_mostlostteam REAL,
		s_mostlostteaminfo TEXT,
		s_fairestgame REAL,
		s_fairestgameinfo TEXT,
		s_unfairestgame REAL,
		s_unfairestgameinfo TEXT,
		s_upset REAL,
		s_upsetinfo TEXT,
		s_highestgame REAL,
		s_highestgameinfo TEXT,
		s_lowestgame REAL,
		s_lowestgameinfo TEXT,
		s_longestwinstreak INTEGER,
		s_longestwinstreakinfo TEXT,
		s_longestlosestreak INTEGER,
		s_longestlosestreakinfo TEXT
		)`);
	db.run(`CREATE TABLE failCmds (
	cmd TEXT
	)`);
});

db.close();
