## Legend

* **command** "required variable" [optional variable] - description of the command  
  * -f/--flags [optional variable for flag] - description of the flag.  

For example:  
* **!rank** [name] - gets you your rank [unless you specify another players name]  
  * -m/--max - prints their maximum rank instead of their current rank  
  * -s/--season "season Number" - prints their rank for the given season  

**the following would be valid rank commands:**  
"!rank" - this would just produce **your current rank**  
"!rank -m" - this would produce your **MAXIMUM** rank  
"!rank --max NizzQ" - this would produce **NizzQ's (a player)** maximum rank  
"!rank -s 2" - this would produce **your final(current)** rank **from season 2**  
"!rank -sm 2 NizzQ" - this would produce **NizzQ's maximum rank he got during season 2**.  
etc.  

# Matchmaking

* **++/!add/!q** [pickup] - adds you to the queue [for that pickup]. ++ adds to all queues, !add/!q with no args adds to the main pickup, or '*' as the pickup arguement will also add to all pickups.  
* **--/!remove** [pickup] - removes you from all queues [or for specified pickup]
* **!who** - prints queues for all current pickups  
  * -g/--global - shows queues from all regions  
  * -r/--region "region" - shows queues in specified region
* **!matches** - prints all current matches  
  * -S/--state - shows what state each match is in  
  * -g/--global - shows matches from all regions  
  * -r/--region "region" - shows matches in a given region  
  * -s/--small - just prints match ids  
* **!match/!teams/!map** [match id] - prints current match [or of given matchid]  
* **!rl** - report loss (if you are captain and you lost, you report it with !rl)  
* **!rc** - report cancel (both team captains need to !rc for a match to cancel)  
* **!rd** - report draw (both team captains need to !rd for a match be be resolved in a draw)  
* **!pick** "@player" [,@Player] - pick a player for your team during match picking stage. You can specify your next pick as well.  
  * **NOTE:** typing "!pick -a" or "!pick --auto" will automatically pick who the bot deems best for your team.  
* **!promote** - Pings the promote role.

# Ranked System

Our ranking system is powered by [OpenSkill](https://github.com/philihp/openskill.js) (works similar to that of [TrueSkill](https://www.microsoft.com/en-us/research/project/trueskill-ranking-system/))

* **!rank** [player] - prints your rank [or for another player].  
  * -m/--max - prints max rank achieved instead of current  
  * -c/--current - prints current rank (this is already default)  
  * -p/--player "player" - specifys a specific player  
  * -g/--global - view from all regions, not just this discord  
  * -a/--alltime - view from all time rank (from all games you have ever played), not just this current season  
  * -r/--region "region" - view rank on a specific region  
  * -d/--discordid "discordid" - use a discordid instead of specifying a players name (good for staff when someone has a name that is difficult to type)  
  * -l/--leagueid "leagueid" - view rank for a given league specified by id.  
  * -s/--season "name/number" - view rank of a given season.  
* **!lb/!leaderboard** [page] - prints leaderboard (page 1 by default)  
  * -m/--max - leaderboard for max rank, not current  
  * -c/--current - current rank not max (this is already default)  
  * -p/--page "page#" - specifys a specific page  
  * -g/--global - view from all regions, not just this discord  
  * -a/--alltime - view from all time ranks (from all games you have ever played), not just this current season  
  * -r/--region "region" - view leaderboard on a specific region  
  * -l/--leagueid "leagueid" - view leaderboard for a given league specified by id.  
  * -s/--season "name/number" - view leaderboard of a given season.  
* **!ranks/!ranks_table** - see what ranks correlate to what elo  

# Other commands

* **!ping** - returns how long it takes bot to recieve your message  
* **!help/!commands** - prints this page  
* **!lastgame**  
  * -g/--global - Last game from any server.
  * -r/--region "region" - Last game from a specific region.
  * -p/--player "player" - View last game for a specific Player.
  * -d/--discordid "discordid" - View last game for a specific player specified by their discordid.
  * -l/--leagueid "leagueid" - View last game for a specific league.
  * -s/--season "season name" - View last game for a specific season.
  * --pickup "pickup name" - View last game for a specific pickup.
* **!stats** 
  * -p/--pickup "pickup" - specify a specific pickup (needs to come after region flag if getting pickup from another region)
  * -r/--region "region" - specify a specific region
  * -g/--global - view from all regions combined.
  * -l/--leagueid "leagueid" - view for a specific leagueid.
  * -s/--season "season" - for a given season
* **!maps** [region] - see current map pool [for a given region]. 
* **!top** [period of time] - See who has played the most amount of games.
  * Times are given as follows: 6Y7M2w9d7h4m = 6 Years, 7 Months, 2 weeks, 9 days, 7 hours, and 4 minutes. 20m = 20 minutes. 14d = 14 days.
  * -s/--season "season" - for a given season.
  * -d/--discord**s** - display discordids instead of player names (for when players have difficult names)
  * -g/--global - from all regions.
  * -r/--region "region" - for a specific region.
  * -a/--alltime - with no timelimit.
  * --page "page#" - !top will only display top 10. Use this if you want to display more than top 10.
  * -l/--leagueid "leagueid" - for a specific leagueid.
  * -p/--pickup "pickup" - for a specific pickup.
  * -t/--time "time" - for a given period of time (see formatting of time above.)

# Administration  

* **!set** "object flag" "setting" "value" - Sets the "setting" of the "object" to value.
  * **NOTE: A full list of settings is contained in the section below**  
  * Can set user variables (e.g. class, country, team, etc.)
  * Can set discord/server variables (e.g. ready time, moderator role, match channel, etc.)
  * Can set pickup variables (e.g. mappool, pick teams, team size, etc.)
  * Can set match state. i.e. what was the result of the match. Can also set for matches already completed. (e.g. change cancelled match to team 1 won.)
* **!createpickup/!addpickup** "name of pickup" - create a new pickup. **Admin Only**.  
* **!noadd/!ban** "@player" [time] [reason] - noadds for a default of 2 hours. Use flags to specify time. Reason is default to last arguement. **Moderator or higher**.  
  * **NOTE: if a given player already has a noadd, then the noadd is updated. This can be used to update the length of their ban, or the reason they were banned. However, it will change the "issuer" of the noadd to the person who last updated the noadd**
  * Times are given as follows: 6Y7M2w9d7h4m = 6 Years, 7 Months, 2 weeks, 9 days, 7 hours, and 4 minutes. 20m = 20 minutes. 14d = 14 days.
  * -p/--permanent - permanent ban  
* **!noadds** [Player] [page#] - view all noadds, [of a certain player, or a differnt page of noadds]  
  * -p/--player "player" - specify player  
  * -d/--discordid "discordid" - specify player by discordid  
  * -h/--historic - view all noadds, including ones previously expired.  
  * -g/--global - view noadds accross all regoins/servers.  
  * -r/--region "region" - view noadds for a specific region  
  * -s/--simple - just display how many per person, and when it expires  
* **!forgive** "@Player" - expires a given players noadd. **Moderator or higher**.  
* **!cfg/!config** [pickup] - View configuration **via dms**. Defaults to discord config [but can specify a pickup]  
  * -p/--pickup "pickup" - specify a pickup  
  * -r/--region/ "region" - view config from another region/server  
* **!newseason** "name/#" - end the current season, and begin a new one. Names cannot have spaces, and are suggested to be as simple as possble. Just do something like "2" instead of "KPC-PUG-SEASON-2:YEAR-OF-THE-DRAGON!". **Admin only**  

# Settings

"!set -f setting value"  
e.g. "!set -u country AU" -> Sets your country to Australia.  
e.g. "!set -d promotedelay 30m" -> Sets the server "!promote" delay to 30 minutes.  
e.g. "!set -p 4v4 mappool sandstorm,undergrowth,site,bureau" -> sets map pool of the 4v4 pickups

## User Settings

These are settings you can set for yourself.  
**NOTE:** you can use any of these flags: "-u", "--user".  

* **country/nation**: Sets your country/nation. Accepts the name of the country or the ISO code exactly as they appear [here](https://en.wikipedia.org/w/index.php?title=Regional_indicator_symbol&useskin=vector).
* **class**: Sets your class. Accepts name of the class or the name of the main weapon. "flex" can be used to show you play multiple classes.
* **team/clan**: Sets your team/clan (they are treated as the same). These are a maximum of 4 characters. Use "LFT" if you are not in a team but are looking for one.

## Discord/Server settings

These are settings for the discord/guild/server. With some exceptions, **these settings are only able to be changed by those with the "admin" role.**  
**NOTE:** you can use any of these flags: "-d", "--discord", "-g", "--guild", "-s", "--server".

* **overlordid**: Sets the absolute authority over the bot. Accepts a discordid or @mention. **Must be set by current "overlord"**.  
* **name**: Sets the simple name of the region. E.g. "EU" or "KPL" or "NACK" or "OCE", etc. Note: bot host can add other aliases that can be used to refer to your server. (e.g. you may set you name to be "KPL", but the bot host can change their config so that "EU" will also point to your server.). **DOES NOT ACCEPT SPACES**.  
* **mainpickupid**: Sets the main pickup (?specified by id?). This is the default pickup shown for "!lb" command, and the default pickup for the "!add" command.  
* **adminrole**: Sets adminrole. Accepts role mention or role id. **Must be set by current "overlord"**.
* **modrole**: Sets modrole. Accepts role mention or role id.
* **readytime**: Sets how long players have to ready react. e.g. "30s".
* **noaddrole**: Sets role which is applied while a user is noadded. Accepts role mention or roleid.
* **channelids**: unimplemented.
* **matchchannel**: Sets the channel that is only accessable when players are in a match. Accepts channel id or channel mention.
* **promotedelay**: Sets how long between "!promote" pings.
* **promoterole**: Sets promote role. Accepts role mention or role id.

## Pickup settings

These are settings for the pickups. **These settings are only able to be changed by those with the "admin" role.**  
**NOTE:** you can use any of these flags: "-p", "--pickip".
**Note:** You then follow the flag by specifiying the pickup e.g. "4v4". You can use the '*' character to specify that you want to change **ALL** pickups.

* **name**: Name of the pickup. No spaces accepted.
* **whiterole**: Sets role required to play this pickup. Accepts role mention or role id.
* **blackrole**: Sets role that will prevent you from playing this pickup. Accepts role mention or role id.
* **mappool**: Sets map pool. Can seperate maps with space or with ','. e.g. "!set -p 4v4 mappool ss,ug,site" == "!set -p 4v4 mappool ss ug site"  
* **pickteams**: Sets the pick team algorithmn: [Detailed Explaination of each option shown here](https://www.youtube.com/watch?v=vuPODkrc3Ec)  
  * "auto": Bot will chose fairest teams via current best practice.  
  * "auto-s"/"autos": Bot will chose fairest teams such that the difference in team elo sum is the smallest.  
  * "auto-p"/"autop": Bot will chose fairest teams via "ABBAABBA" pick order (or appropirate pick order for given team size).
  * "auto-i"/"autoi": Bot will chose fairest teams via first balancing individual matchups (i.e. best player will have the 2nd best player on opposite team. Worst player will have the 2nd worst player on opposite team, etc.), then chosing fairest game (via elo summing) with these restrictions. **The purpose of this mode is to avoid situaions in which there is 1 great player, trying to carry 3 horrible players against a team of 3-4 good/average players.
  * "manual": Captians will select their teams via "ABBAABBA" pick order (or appropirate pick order for given team size).
  * "manual-o"/"manualo": Captains will select their teams via pick order chosen by "auto-i" method.
  * "exp"/"experimental": Will randomly choose one of the modes above for each match (each match will have different way of picking teams). This will allow users and staff to experiment and test all pick modes and find what works best for their server.
* **ranked**: 0/1 or true/false. Turn ranked on/off.
* **teamsize**: set teamsize.

## Match setting/reporting.

These are for changing outcomes of matches. **Requires Moderator Role.**  
**NOTE:** you can use any of these flags: "-m", "--match".  
You then follow the flag by specifiying the match id.  
And then follow that with the result of the match:  
  * 1 = team 1 won  
  * 2 = team 2 won  
  * d = draw  
  * c = cancel  

