var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('maindb.sqlite3');

function te(err, from="unknown"){
	if(err) throw new Error(""+from+": "+err);
}

function initStats(cb){
	db.get("SELECT * FROM stats", function(err, row){
		te(err, "initstats");
		cb(row);
	});
}

function initGuilds(cb1, cb2, cbr){
	db.each("SELECT * FROM guilds", function(er1, ro){
		te(er1, "initGuilds 1");
		cb1(ro);
	}, function(er, rows){
		te(er, "initguilds 2");
		db.each("SELECT * FROM pickups", function(err, rows1){
			te(err,"initguilds 3");
			cb2(rows1);
		}, function(errr, rows2){
			te(errr, "initguilds 4");
			cbr(rows2);
		});
	});
}

function createGuild(guildId, overlord, channelId, leagueId){
	db.run("INSERT OR REPLACE INTO guilds(guildid, overlordid, channelids, leagueid) VALUES ((?), (?), (?), (?))", guildId, overlord, channelId.toString(), leagueId, te);
	db.run("INSERT OR REPLACE INTO leagues(leagueid, name, starttime, guildid) VALUES (?, ?, ?, ?)", leagueId, guildId, Date.now(), guildId,te);
}

function setGuild(guildid, setting, value){
	switch(setting){
		case "overlordid": case "name": case "mainpickupid": case "adminrole": case "modrole": case "readytime": case "noaddrole": case "channelids": case "matchchannel": case "promotedelay": case "promoterole": case "placementgames":
			break;
		default: throw new Error("SQLINJECT??? Unexpected guild setting \""+setting+"\"");
	}
	db.run("UPDATE guilds SET "+setting+" = ? WHERE guildid = ?", value, guildid, te);
}

function setPickup(pickupid, setting, value){
	switch(setting){
		case "name": case "whiterole": case "blackrole": case "mappool": case "pickteams": case "ranked": case "teamsize": case "placementgames":
			break;
		default: throw new Error("SQLINJECT??? Unexpected pickup setting \""+setting+"\"");
	}
	db.run("UPDATE pickups SET "+setting+" = ? WHERE pickupid = ?", value, pickupid, te);
}

function createPickup(info, mainpickup) {
	db.run("INSERT OR REPLACE INTO pickups(name,pickupid,guildid,leagueid) VALUES (?, ?, ?, ?)", info.name, info.pickupid, info.guildid, info.leagueid, te);
	db.run("INSERT OR REPLACE INTO leagues(leagueid, name, starttime, guildid, pickupid) VALUES (?, ?, ?, ?, ?)", info.leagueid, "0", Date.now(), info.guildid, info.pickupid,te);
	if(mainpickup) db.run("UPDATE guilds SET mainpickupid = ? WHERE guildid = ?", info.pickupid, info.guildid,te);
}

function updateStats(stats){
	db.run("UPDATE stats SET nextmatchid = ?, nextplayerid = ?, nextleagueid = ?, nextpickupid = ?", stats.nextmatchid, stats.nextplayerid, stats.nextleagueid, stats.nextpickupid,te);
}

function updateNick(discordid, nickname){
	db.run("INSERT OR REPLACE INTO players(discordid, nickname) VALUES (?, ?)", discordid, nickname, te);
}

function reportMatch(match){
	matchVals = {
		$matchid: match.matchid,
		$pickupid: match.pickupid,
		$t1: match.teams[0].join(','),
		$t2: match.teams[1].join(','),
		$result: match.result,
		$starttime: match.startTime,
		$map: match.map,
		$team1sum: match.teamSum[0],
		$team2sum: match.teamSum[1]
	};
	db.run("INSERT OR REPLACE INTO matches(matchid, pickupid, team1list, team2list, result, starttime, map, team1sum, team2sum) VALUES ($matchid, $pickupid, $t1, $t2, $result, $starttime, $map, $team1sum, $team2sum)", matchVals, te);
	for(var did in match.players){
		updateNick(did, match.players[did].nickname);
	}
}

function updateRank(data){//updates rank AND elochanges
	rankValues = {
		$discordid: data.discordid,
		$leagueid: data.leagueid,
		$mu: data.mu,
		$sig: data.sig,
		$maxmu: data.maxmu,
		$maxsig: data.maxsig,
		$wins: data.wins,
		$losses: data.losses,
		$draws: data.draws,
		$streak: data.streak
	};
	db.run("INSERT OR REPLACE INTO ranks(discordid, leagueid, mu, sig, maxmu, maxsig, wins, losses, draws, streak) VALUES ($discordid, $leagueid, $mu, $sig, $maxmu, $maxsig, $wins, $losses, $draws, $streak)",rankValues,te);
	elochangeValues = {
		$matchid: data.matchid,
		$discordid: data.discordid,
		$leagueid: data.leagueid,
		$muchange: data.muchange,
		$sigchange: data.sigchange,
		$class: data.class
	};
	db.run("INSERT OR REPLACE into elochanges(matchid, discordid, leagueid, muchange, sigchange, classes) VALUES ($matchid, $discordid, $leagueid, $muchange, $sigchange, $class)", elochangeValues,te);

}

function getRank(authorId, leagueId, cb) {
	db.get("SELECT mu, sig, maxmu, maxsig, wins, losses, draws, streak FROM ranks WHERE discordid = ? AND leagueid = ?", authorId, leagueId, function(er,ro){
		te(er,"getRank");
		cb(ro);
	});
}

function getLB(options, cb){
	values = {
		$offset: (options.page-1)*10,
	};
	query = "SELECT ranks.discordid, nickname, country, ";
	max = false;
	if(options.hasOwnProperty("current") || !options.hasOwnProperty("max")){
		query+="mu, sig, ";
	}else{
		max = true;
		query+="maxmu, maxsig, ";
	}
	query += "wins, losses, draws FROM ranks INNER JOIN players USING(discordid) INNER JOIN leagues USING(leagueid) WHERE leagueid ";

	if(options.hasOwnProperty("global")){
		query+= "IN (SELECT leagueid FROM guilds) ";
	}else if(options.hasOwnProperty("guildid")){
		values.$guildid = options.guildid;
		query += "= (SELECT leagueid FROM ";
		if(options.hasOwnProperty("season")){
			values.$season = options.season;
			query += "leagues WHERE name = $season AND guildid = $guildid ) ";
		}else{//requested alltime or region
			query +="guilds WHERE guildid = $guildid ) ";
		}
	}else{
		values.$leagueid = options.leagueid;
		query += "= $leagueid ";
	}
	query += "AND (wins+losses+draws)>= placementgames "; 
	query += "ORDER BY ranks.";
	if(max) query+="max";
	query += "mu desc LIMIT 10 OFFSET $offset";
	db.all(query, values, function(er, ro){
		te(er,"getLB");
		cb(ro);
	});
}

function getDetailedRank(options, cb){
	query = "SELECT nickname, mu, sig, maxmu, maxsig, wins, losses, draws, streak, placementgames, country, class, team FROM ranks INNER JOIN players USING(discordid) INNER JOIN leagues USING (leagueid) ";
	values = {};
	if(options.hasOwnProperty("player")){
		query+="WHERE nickname = $nickname AND ";
		values.$nickname = options.player;
	}else{
		query+="WHERE discordid = $discordid AND ";
		values.$discordid = options.discordid;
	}
	hasGlobal = options.hasOwnProperty("global");
	hasGuildid = options.hasOwnProperty("guildid");
	hasLeagueid = options.hasOwnProperty("leagueid");
	hasSeason = options.hasOwnProperty("season");
	if(hasLeagueid){
		query+="leagueid = $leagueid ";
		values.$leagueid = options.leagueid;
	}else if(hasSeason){
		query+="name = $season AND guildid = $guildid ";
		values.$season = options.season;
		values.$guildid = options.guildid;
	}else if(hasGlobal || hasGuildid){
		query+=" leagueid IN (SELECT leagueid FROM guilds ";
		if(hasGuildid) {
			values.$guildid = options.guildid;
			query+="WHERE guildid=$guildid ";
		}
		query+=") ";
	}
	db.get(query,values,function(err, row){
		if(err) throw new Error(err);
		if(row==null) cb(row);
		else if(row.wins+row.losses+row.draws<row.placmentgames) cb(row);
		else{
			posVals = {};
			posQuery = "SELECT COUNT(*) FROM ranks INNER JOIN leagues USING (leagueid) WHERE (wins+losses+draws)>=placementgames AND ";
			if(options.hasOwnProperty("max")) {
				posQuery+="maxmu ";
				posVals.$mu = row.maxmu;
			}else{
				posQuery+="mu ";
				posVals.$mu = row.mu;
			}
			posQuery+="> $mu AND ";

			if(hasLeagueid){
				posQuery+="leagueid = $leagueid ";
				posVals.$leagueid = options.leagueid;
			}else if(hasSeason){
				posQuery+="name = $season AND guildid = $guildid ";
				posVals.$season = options.season;
				posVals.$guildid = options.guildid;
			}else if(hasGlobal || hasGuildid){
				posQuery+=" leagueid IN (SELECT leagueid FROM guilds ";
				if(hasGuildid) {
					posVals.$guildid = options.guildid;
					posQuery+="WHERE guildid=$guildid ";
				}
				posQuery+=") ";
			}
			db.get(posQuery,posVals,function(er,ro){
				if(er) throw new Error(er);
				row.pos = ro["COUNT(*)"]+1;
				cb(row);
			});
		}
	});

}

function incMatchid(nextmatchid){
	db.run("UPDATE stats SET nextmatchid = ?", nextmatchid,te);
}

function getInfractions(options, cb){
	//duration == -1 => permanent ban
	values = {
		$guildid: options.guildid,
		$offset: (options.page-1)*10
	};
	query="SELECT infractions.*, players.nickname FROM infractions JOIN players on infractions.discordid = players.discordid WHERE guildid = $guildid ";
	if(!options.hasOwnProperty("historic")){
		values.$timenow = Date.now();
		query+="AND ((issuetime+duration > $timenow) OR duration < 0) AND forgiven IS NULL ";
	}
	query+="ORDER BY issuetime desc LIMIT 10 OFFSET $offset";
	db.all(query, values, function(er,ro){
		te(er,"getInfractions");
		cb(ro);
	});
}

function totalStats(options, cb){
	inputs = {};
	query = "SELECT matches.result='d', COUNT(*) FROM matches ";
	join = "";
	where = "";
	hasGuildid = options.hasOwnProperty("guildid");
	hasPickups = options.hasOwnProperty("pickup");
	hasLeagueid = options.hasOwnProperty("leagueid");
	hasSeason = options.hasOwnProperty("season");
	if(hasGuildid || hasPickups){
		join+="JOIN pickups USING (pickupid) ";
		if(hasGuildid) {
			where+="WHERE pickups.guildid = $guildid ";
			inputs.$guildid = options.guildid;
		}
		if(hasPickups){
			if(!hasGuildid) where +="WHERE ";
			else where+="AND ";
			where+="pickups.name = $pickup ";
			inputs.$pickup = options.pickup;
		}
	}
	if(hasLeagueid || hasSeason) {
		join += "JOIN (SELECT DISTINCT matchid, leagueid FROM elochanges WHERE leagueid=";
		if(hasSeason){
			join+="(SELECT leagueid FROM leagues WHERE name= $season ";
			if(hasGuildid){
				join+="AND guildid=$guildid) ";
				inputs.$guildid = options.guildid;
			}else join+=") ";
			inputs.$season = options.season;
		}else {
			join+="$leagueid";
			inputs.$leagueid = options.leagueid;
		}
		join += ") USING(matchid) ";

	}
	query+=join+where+"GROUP BY matches.result = 'd'";
	db.all(query, inputs, function(er,ro){
		te(er,"totalStats");
		cb(ro);
	});
}

function close(cb) {db.close(cb);}

function recCmd(message){
	db.run("INSERT INTO failCmds(cmd) VALUES (?)", message,te);
}

function getLastgame(options, cb){
	values = {};
	query = "SELECT matchid, pickupid, team1list, team2list, result, starttime FROM matches ";
	whereClause = false;

	hasPlayer = options.hasOwnProperty("player");
	hasDiscordid = options.hasOwnProperty("discordid");
	hasLeagueid = options.hasOwnProperty("leagueid");
	hasSeason = options.hasOwnProperty("season");
	hasGuildid = options.hasOwnProperty("guildid");
	hasPickup = options.hasOwnProperty("pickup");
	if(hasPlayer || hasDiscordid || hasLeagueid || hasSeason){
		whereClause = true;
		query += "WHERE matchid IN (SELECT matchid FROM elochanges WHERE ";
		if(hasPlayer || hasDiscordid){
			query+="discordid = ";
			if(hasPlayer) {
				values.$nickname = options.player;
				query+="(SELECT discordid FROM players WHERE nickname = $nickname) ";
			}else{
				values.$discordid = options.discordid;
				query +="$discordid ";
			}
			if(hasLeagueid || hasSeason) query+="AND ";
			else query+=") ";
		}
		if(hasLeagueid || hasSeason){
			query+="leagueid = ";
			if(hasLeagueid){
				values.$leagueid = options.leagueid;
				query+="$leagueid) ";
			}else{
				values.$season = options.season;
				query+="(SELECT leagueid FROM leagues WHERE name=$season)) ";
			}
		}
	}
	if(hasGuildid || hasPickup){
		if(whereClause) query+="AND ";
		else query+="WHERE ";
		query +="pickupid = (SELECT pickupid FROM pickups WHERE leagueid IN (SELECT leagueid FROM ";
		if(hasPickup){
			values.$pickup = options.pickup;
			query+="pickups WHERE name = $pickup) "; 
		}else{
			values.$guildid = options.guildid;
			query+="leagues WHERE guildid = $guildid) "; 
		}
		query+=") ";
	}
	query+="ORDER BY matchid DESC";
	db.get(query,values,function(err,row){
		te(err,"getLastGame 1");
		if(row==null) return cb(null);
		row.players = {};
		row.teams = new Array(2);
		row.teams[0] = row.team1list.split(',');
		row.teams[1] = row.team2list.split(',');
		l = row.teams[0].length;
		query2 = "SELECT discordid, nickname FROM players WHERE discordid IN ( ?";
		for(let i=1;i<l*2;i++){
			query2+=", ?";
		}
		query2 +=" )";
		db.each(query2,row.teams[0].concat(row.teams[1]),function(er, ro){
			te(er,"getLastGame 2");
			row.players[ro.discordid]={
				nickname: ro.nickname
			};
		}, function(er, ro){
			te(er,"getLastGame 3");
			cb(row);
		});
	});
}

function getPlacementGames(leagueid, cb){
	db.get("SELECT placementgames,leagueid FROM leagues WHERE leagueid = ?", leagueid, function(er,ro){
		te(er,"getPlacementGames");
		cb(ro);
	});
}

function addInfraction(discordid, guildid, duration, reason, issuer, cb){
	if(reason=='') reason=null;
	else reason = reason.slice(0,-1); //our reasons have a trailing space that we remove
	getVals = {
		$discordid: discordid,
		$guildid: guildid,
		$timenow: Date.now()
	};
	restrictions = "discordid=$discordid AND guildid=$guildid AND ((issuetime+duration)>$timenow OR duration==-1) AND forgiven IS NULL";
	db.get("SELECT reason, duration, issuetime FROM infractions WHERE "+restrictions,getVals, function(err, row){
		te(err,"addInfraction");
		newVals = {
			$issuetime: Date.now(),
			$duration: 7200000,
			$reason: reason,
			$issuer: issuer,
			$discordid: discordid,
			$guildid: guildid
		};
		if(duration!=null) newVals.$duration = duration;
		if(row==null){
			cb(true, newVals);
			db.run("INSERT INTO infractions (discordid, guildid, issuetime, duration, reason, issuer) VALUES ($discordid, $guildid, $issuetime, $duration, $reason, $issuer)",newVals,te);
		}else{
			newVals.$timenow = Date.now();
			if(row.reason!=null && reason==null) newVals.$reason=row.reason;
			if(duration==null) {
				newVals.$duration = row.duration;
				newVals.$issuetime = row.issuetime;
			}
			cb(false, newVals);
			db.run("UPDATE infractions SET issuetime=$issuetime, duration=$duration, reason=$reason, issuer=$issuer WHERE "+restrictions, newVals,te);
		}
	});
}

function forgiveInfraction(discordid, guildid, forgiver, cb){
	vals = {
		$forgiver: forgiver,
		$discordid: discordid,
		$guildid: guildid,
		$timenow: Date.now()
	}
	db.run("UPDATE infractions SET forgiven = $forgiver WHERE discordid=$discordid AND guildid=$guildid AND ((issuetime+duration)>$timenow OR duration==-1) AND forgiven IS NULL", vals, function(err){
		if(err) throw new Error(err);
		cb();
	});
}

function checkInfraction(discordid, guildid, banned, unbanned){
	vals = {
		$discordid: discordid,
		$guildid: guildid,
		$currenttime: Date.now()
	};
	db.get("SELECT duration, issuetime FROM infractions WHERE discordid=$discordid AND guildid=$guildid AND ((issuetime+duration)>$currenttime OR duration==-1) AND forgiven IS NULL",vals,function(err,row){
		te(err,"checkInfraction");
		if(row==null) unbanned();
		else banned(row.duration, row.issuetime);
	});
}

function getTop(options, cb){
	values = {};
	query = "SELECT COUNT(*), ";
	if(options.hasOwnProperty("discordids")) query+="discordid ";
	else query+="nickname ";
	query += "FROM matches INNER JOIN elochanges USING(matchid) INNER JOIN players USING(discordid) ";
	whereClause = false;
	hasLeagueid = options.hasOwnProperty("leagueid");
	hasGuildid = options.hasOwnProperty("guildid");
	hasPickup = options.hasOwnProperty("pickup");
	if(options.hasOwnProperty("season")){
		values.$season = options.season;
		query+="WHERE leagueid = (SELECT leagueid FROM leagues WHERE name=$season) ";
		whereClause = true;
	}else if(hasLeagueid){
		values.$leagueid = options.leagueid;
		query+="WHERE leagueid = $leagueid ";
		whereClause = true;
	}
	if(hasGuildid || hasPickup){
		if(whereClause) query+="AND ";
		else query+="WHERE ";
		query+="pickupid IN (SELECT pickupid FROM pickups WHERE ";
		if(hasPickup){
			values.$pickup = options.pickup;
			query+="name=$pickup ";
			if(hasGuildid) query+="AND ";
		}
		if(hasGuildid){
			values.$guildid = options.guildid;
			query+="guildid=$guildid ";
		}
		query+=") ";
		whereClause = true;
	}
	if(options.hasOwnProperty("time")){
		values.$starttime = options.time;
		if(whereClause) query+="AND ";
		else query+="WHERE ";
		query +="starttime>$starttime ";
	}
	query+="GROUP BY nickname ORDER BY COUNT(*) DESC LIMIT 10 ";
	if(options.hasOwnProperty("page")){
		values.$offset = (options.page-1)*10;
		query+="OFFSET $offset";
	}
	db.all(query, values, function(er,row){
		te(er,"getTop");
		cb(row);
	});
}

function getMatch(matchid, cb){
	query = "SELECT pickupid, team1list, team2list, result, starttime, team1sum, team2sum FROM matches WHERE matchid = ?";
	db.get(query,matchid,function(err,row){
		te(err,"getMatch");
		if(row==null) return cb(null);
		row.players = {};
		row.teams = new Array(2);
		row.teams[0] = row.team1list.split(',');
		row.teams[1] = row.team2list.split(',');
		row.teamSum = [
			row.team1sum,
			row.team2sum
		]
		l = row.teams[0].length;
		query2 = "SELECT discordid, nickname FROM players WHERE discordid IN ( ?";
		for(let i=1;i<l*2;i++){
			query2+=", ?";
		}
		query2 +=" )";
		db.each(query2,row.teams[0].concat(row.teams[1]),function(er, ro){
			if(er) throw new Error(er);
			row.players[ro.discordid]={
				nickname: ro.nickname
			};
		}, function(er, ro){
			if(er) throw new Error(er);
			cb(row);
		});
	});
}

function setUser(discordid, setting, value){
	//CAN NEVER BE TOO SAFE
	switch(setting){
		case "country": case "class": case "team":
			break;
		default:
			throw new Error("SQL INJECTION ATTACK VULN: d.setUser contained setting = "+setting);
	}
	db.run("UPDATE players SET "+setting+"= ? WHERE discordid = ?", value, discordid,te);
}

function recreateRanks(match, cb){
	players = {};
	first = null;
	db.each("SELECT * FROM elochanges INNER JOIN ranks USING(discordid, leagueid) WHERE matchid = ?", match.matchid, function(err,row){ 
		if(err) throw new Error(err);
		if(!players.hasOwnProperty(row.discordid)){
			players[row.discordid]={
				leagues: {}
			};
		}
		players[row.discordid].leagues[row.leagueid]={
			mu: row.mu-row.muchange,
			sig: row.sig-row.sigchange,
			maxmu: row.maxmu,
			maxsig: row.maxsig,
			wins: row.wins,
			losses: row.losses,
			draws: row.draws,
			streak: row.streak
		};
	}, function(err, row){
		if(err) throw new Error(err);
		cb(players);
	});
}

function setPlacementgames(leagueid, placementgames){
	db.run("UPDATE leagues SET placementgames = ? WHERE leagueid = ?", placementgames, leagueid,function(err){
		if(err) throw new Error(err);
	});
}

module.exports = {
	initStats,
	initGuilds,
	createGuild,
	setGuild,
	createPickup,
	reportMatch,
	getRank,
	getLB,
	getDetailedRank,
	incMatchid,
	getInfractions,
	close,
	totalStats,
	recCmd,
	getLastgame,
	getPlacementGames,
	updateNick,
	updateRank,
	addInfraction,
	forgiveInfraction,
	checkInfraction,
	getTop,
	getMatch,
	setUser,
	recreateRanks,
	setPickup,
	updateStats,
	setPlacementgames
};
