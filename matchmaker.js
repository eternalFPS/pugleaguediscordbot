const {Player, Match} = require('./structs.js');
const {MessageEmbed} = require('discord.js');
const u = require('./utils.js');
const rs = require('./ranksys');
const d = require("./dbreqs.js");

////////////////////////////////
// Match Birth
////////////////////////////////

function addQueue(guilds, message, args, timers){
	guildid = message.guildId;
	guild = guilds[guildid];
	authorId = message.author.id;
	d.checkInfraction(authorId,guildid,function(duration, issuetime){
		//THEY ARE BANNED
		outMsg = "";
		if(duration==-1) outMsg = "You are permanently banned";
		else outMsg = "You are banned! (Expires in "+u.toTimeString(duration+issuetime-Date.now())+")";
		u.reply(message, outMsg);
		
	}, function(){
		//NOT BANNED
		[throwaway,match] = playersMatch(guild, authorId);
		if(match!==null){
			u.reply(message, `You are currently in match ${match}`);
			return;
		}
		added = false;
		for(var p in guild.pickups){
			pickup = guild.pickups[p];
			if (args.length > 1 && !args.includes(pickup.name)) continue;
			if(!(pickup.queue.includes(authorId))){
				added = true;
				pickup.queue.push(authorId);

				//adding (iformation/new players) to active players
				active = guild.active;
				leagueids = [pickup.leagueid, guild.leagueid];
				nickname = message.member.nickname;
				if(nickname===null) nickname = message.author.username;
				addActive(active, nickname, authorId, leagueids, function(){
					teamsize = pickup.settings.teamsize;
					if (pickup.queue.length >= teamsize*2) {
						rdyMsg = "**Match Ready**, BUT ARE YOU???\n";
						for(let i=0;i<pickup.queue.length;i++){
							rdyMsg+="<@"+pickup.queue[i]+"> ";
						}
						rdyMsg +="\nReact with ✅ to mark yourself ready to play!";
						message.channel.send({
							content: rdyMsg,
							allowedMentions: {parse:["users"]}
						}).then(function(msg){
							msg.react('✅').then(function(){
								msg.react('⛔');
							});
							pickup.ready = {
								msgid:msg.id,
								players: new Array(teamsize*2),
								aLength:0
							}
						});
						nTimers = new Array(timers.length+1);
						time = Date.now()+guild.settings.readytime;
						ni = 0;
						for(let i=0;i<timers.length;i++){
							if(timers[i].time<time){
								nTimers[ni++]=timers[i];
							}else{
								nTimers[ni++]={
									time: time,
									exe: "mm.readyFail",
									input:{
										guildid: guildid,
										pickupid: p
									}
								}
							}
						}
					}else{
						u.reply(message,stringQueues(guild), "Printed Queues");
					}
				});
				break;
			}
		}
		if(args.length>1 && !added) u.reply(message, "No such pickup \""+args[1]+"\"");
	});
}

function requireReady(message, guild, queue){
	message.reply("test").then(function(msg) {
		msg.react('✅').then(function(){
			msg.react('⛔');
		});
		guild.waitingReact[msg.id]=[
			queue.slice(),
			new Array(queue.length)
		];
	});
}

function readyFail(guild, pickupid, channel){
	//TODO edit old message to say players didn't react
	if(!guild.pickups.hasOwnProperty(pickupid)) throw new Error("my brains fried cant think of what error this is");
	pickup = guild.pickups[pickupid];
	nuQ = new Array(pickup.ready.aLength);
	for(let i=0;i<pickup.ready.aLength;i++){
		nuQ[i] = pickup.ready.players[i];
	}
	pickup.queue = nuQ;
	pickup.ready = {
		msgid: null,
		players: null,
		alength: 0
	};
	outMsg = "**Match unable to start because not enough players were Ready**\n";
	outMsg+=stringQueues(guild);
	channel.send({
		content: outMsg,
		allowedMentions: {parse:[]}
	});
}

function createMatch(guilds, pickup, matchid) {
	guildid = pickup.guildid;
	guild = guilds[guildid];
	teamsize = pickup.settings.teamsize;
	queue = pickup.queue;
	scratch = queue.slice(0,teamsize*2);//incase of overqueue
	teamSum = [0,0];
	teams = [new Array(teamsize), new Array(teamsize)];

	vals = {
		state: 2,
		players: {},
		matchid: matchid,
		pickupid: pickup.pickupid,
		leagueids: [pickup.leagueid, guild.leagueid],
	};

	if(teamsize>1){//team picking for games that arn't 1v1s
		scratch.sort(function(a,b){
			return guild.active[b].leagues[pickup.leagueid].rank.mu - guild.active[a].leagues[pickup.leagueid].rank.mu; //b - a, because we want highest
		});
		ti = [0,0];//counter for first empty index of the above teams array
		// TEAM PICKING
		howpick = pickup.settings.pickteams;
		if(howpick=="exp"){
			switch(u.randInt(1,5)){
				case 1:
					howpick = "autos";
					break;
				case 2:
					howpick = "autop";
					break;
				case 3:
					howpick = "autoi";
					break;
				case 4:
					howpick = "manual";
					break;
				case 5:
					howpick = "manualo";
					break;
			}
			vals.pickteams = howpick;
		}
		switch(howpick){
			case "auto-s": case "autos":
				bestDiff=99999999;
				bestTeam=null;
				len=teamsize*2;
				indext1= new Array(teamsize);
				indext1[0]=0;
				//is declaring a function like this bad practice? idk
				function teamGen(start,depth){
					if(depth>teamsize-1){
						sum = [0,0];
						cmpInd = 0;
						for(let i=0;i<len;i++){
							if(cmpInd<teamsize && indext1[cmpInd]==i){
								sum[0]+=guild.active[scratch[i]].leagues[pickup.leagueid].rank.mu;
								cmpInd++;
							}else sum[1]+=guild.active[scratch[i]].leagues[pickup.leagueid].rank.mu;
						}
						diff=Math.abs(sum[0]-sum[1]);
						if(diff<bestDiff){
							bestDiff = diff;
							teamSum = [sum[0],sum[1]];
							bestTeam = indext1.slice();
						}
					}else{
						for(let i=start;i<teamsize+1+depth;i++){
							indext1[depth]=i;
							teamGen(i+1,depth+1);
						}
					}
				}
				teamGen(1,1);
				t1Ind = 0;
				for(let i=0;i<len;i++){

					if(bestTeam[t1Ind]==i){
						teams[0][ti[0]++] = scratch[i];
						t1Ind++;
					}else{
						teams[1][ti[1]++] = scratch[i];
					}
				}
				break;
			case "autop": case "auto-p":
				//this is our ABBAABBA pick order for any team size
				reps = 2*Math.floor(teamsize/2);
				for(let i=0;i<reps;i++){
					team = 1;
					x = i%4;
					if(x==0||x==3) team = 0;
					teams[team][ti[team]] = scratch[i];
					teams[team][teamsize-1-ti[team]++] = scratch[teamsize*2-1-i];
					teamSum[team]+=guild.active[scratch[i]].leagues[pickup.leagueid].rank.mu;
					teamSum[team]+=guild.active[scratch[teamsize*2-1-i]].leagues[pickup.leagueid].rank.mu;
				}
				//cannot balance odd length'd teams, so rng saves the day
				if(reps!=teamsize){
					team=1;
					if(Math.round(Math.random())) team=0;
					teams[team][ti[team]]=scratch[teamsize-1];
					teams[1-team][ti[1-team]]=scratch[teamsize];
					teamSum[team]+=guild.active[scratch[teamsize-1]].leagues[pickup.leagueid].rank.mu;
					teamSum[team]+=guild.active[scratch[teamsize]].leagues[pickup.leagueid].rank.mu;
				}
				ti[0]=4;
				ti[1]=4;//probably does nothing, but just in case
				break;
			case "autoi": case "auto-i": case "auto":
				bestDiff = 9999999999;
				bestTeam = 0;
				for(let i=0;i<(2**teamsize);i++){
					div = Math.floor(i/2);
					mod = i%2;
					ind = 0;
					sum = [0,0];;
					while(ind<2*teamsize-1){
						sum[0]+=guild.active[scratch[ind+mod]].leagues[pickup.leagueid].rank.mu;
						sum[1]+=guild.active[scratch[ind+1-mod]].leagues[pickup.leagueid].rank.mu;
						div=Math.floor(div/2);
						mod=div%2;
						ind+=2;
					}
					diff=Math.abs(sum[0]-sum[1]);
					if(diff<bestDiff){
						bestDiff=diff;
						teamSum = [sum[0],sum[1]];
						bestTeam=i;
					}
				}
				//then once we have found best team, recreate that team
				div = Math.floor(bestTeam/2);
				mod = bestTeam%2;
				ind = 0;
				while(ind<2*teamsize-1){
					teams[0][ti[0]++] = scratch[ind+mod];
					teams[1][ti[1]++] = scratch[ind+1-mod];
					div=Math.floor(div/2);
					mod=div%2;
					ind+=2;
				}
				break;
			case "manual":
				vals.state = 1;
				teams[0][0]=scratch[0];
				teams[1][0]=scratch[1];
				teamSum[0]=guild.active[scratch[0]].leagues[pickup.leagueid].rank.mu;
				teamSum[1]=guild.active[scratch[1]].leagues[pickup.leagueid].rank.mu;
				//this is our ABBAABBA pick order for any team size
				pickorder = new Array((teamsize-1)*2);
				reps = 2*Math.floor(teamsize/2);
				for(let i=0;i<reps;i++){
					team = 1;
					x = i%4;
					if(x==0||x==3) team = 0
					if(i>=2) pickorder[i-2] = team;
					pickorder[2*teamsize-3-i] = team;
				}
				//cannot balance odd length'd teams, so rng saves the day
				if(reps!=teamsize){
					if(Math.round(Math.random())){
						pickorder[teamsize-1]=0;
						pickorder[teamsize]=1;
					}else{
						pickorder[teamsize-1]=1;
						pickorder[teamsize]=0;
					}
				}
				vals.scratch = scratch.slice(2);
				vals.pickorder = pickorder;
				vals.teamind = [1,1];
				break;
			case "manualo": case "manual-o":
				//mostly copy pasted code from auto-i, so if this or that is broken, update both accordingly
				vals.state=1;
				bestDiff = 9999999999;
				bestTeam = 0;
				for(let i=0;i<(2**teamsize);i++){
					div = Math.floor(i/2);
					mod = i%2;
					ind = 0;
					sum = [0,0];;
					while(ind<2*teamsize-1){
						sum[0]+=guild.active[scratch[ind+mod]].leagues[pickup.leagueid].rank.mu;
						sum[1]+=guild.active[scratch[ind+1-mod]].leagues[pickup.leagueid].rank.mu;
						div=Math.floor(div/2);
						mod=div%2;
						ind+=2;
					}
					diff=Math.abs(sum[0]-sum[1]);
					if(diff<bestDiff){
						bestDiff=diff;
						bestTeam=i;
					}
				}
				//then once we have found best team, recreate that pick order
				pickorder = new Array((teamsize-1)*2);

				div = Math.floor(bestTeam/2);
				mod = bestTeam%2;
				ind = 0;
				while(ind<2*teamsize-1){
					if(ind==0){
						teams[0][ti[0]++] = scratch[ind+mod];
						teams[1][ti[1]++] = scratch[ind+1-mod];
						teamSum[0]=guild.active[scratch[ind+mod]].leagues[pickup.leagueid].rank.mu;
						teamSum[1]=guild.active[scratch[ind+1-mod]].leagues[pickup.leagueid].rank.mu;
					}else{
						pickorder[ind+mod-2]=0;
						pickorder[ind+1-mod-2]=1;
					}
					div=Math.floor(div/2);
					mod=div%2;
					ind+=2;
				}

				vals.scratch = scratch.slice(2);
				vals.pickorder = pickorder;
				vals.teamind = [1,1];
				break;
		}
	}else{//1v1
		for(let i=0;i<2;i++){
			teams[i][0] = scratch[i];
			teamSum[i] = guild.active[scratch[i]].leagues[pickup.leagueid].rank.mu;
		}
	}
	vals.teams = teams;
	vals.teamSum = teamSum;
	//dont want to add this loop, but it helps make the team picking code more readable
	for(let i=0;i<teamsize*2;i++){
		vals.players[scratch[i]] = guild.active[scratch[i]];
		guild.active[scratch[i]].queuedAt = null;
		guild.active[scratch[i]].matchId = matchid;
		for(var gid in guilds){
			removeQueue(guilds[gid],null,scratch[i]);
		}

	}
	mappool = pickup.settings.mappool;
	if(mappool.length>0) vals.map = mappool[u.randInt(0,mappool.length-1)];
	return new Match(vals);
}

function addActive(active, nickname, authorId, leagueids, cb){
	if(!active.hasOwnProperty(authorId)){
		leagues = {};
		rs.getLeague(authorId, leagueids[0], function(lleague){
			leagues[leagueids[0]] = lleague;
			rs.getLeague(authorId, leagueids[1], function(rleague){
				leagues[leagueids[1]] = rleague;
				active[authorId] = new Player({
					leagues:leagues,
					nickname: nickname
				});
				cb();
			});
		});
	}else{
		active[authorId].nickname = nickname;
		cb();
	}
}

function pick(message, args, guild){
	pid = message.author.id;
	[p,m] = playersMatch(guild,pid);
	if(m==null) return u.reply(message, "You are not in a match");
	match = guild.pickups[p].matches[m];
	if(match.state!=1) return u.reply(message, "Match is not in the pick teams state");
	team = -1;
	for(let i=0;i<2;i++){
		if(match.teams[i][0]==pid) team = i;
	}
	if(team==-1) return u.reply(message, "You are not a captain");
	if(args.length<2) return u.reply(message, "You did not specify what player you were picking");
	settings = guild.pickups[p].settings;
	i = settings.teamsize*2-2-match.scratch.length;
	if(pickorder[i]!=team) return u.reply(message, "Not your turn to pick");
	chosen = '';
	if(args[1]=='a'||args[1]=="auto"){
		chosen = match.scratch.shift();
	}else{
		mention = u.idFromMention(args[1]);
		if(mention=='') return u.reply(message, "You need to mention the player you are trying to pick");
		newScratch = new Array(match.scratch.length-1);
		nsi = 0;
		for(let i=0;i<match.scratch.length;i++){
			if(match.scratch[i]==mention){
				chosen = mention;
			}else if(nsi>=newScratch.length) {
				return u.reply(message, "That player is not in the list of unpicked players");
			}else{
				newScratch[nsi++] = match.scratch[i];
			}
		}
		match.scratch = newScratch;
	}
	match.teamSum[team]+=match.players[chosen].leagues[guild.pickups[p].leagueid].rank.mu;
	match.teams[team][match.teamind[team]++] = chosen;
	//only one player left
	if(match.scratch.length==1){
		team = pickorder[++i];
		chosen2 = match.scratch.shift();
		match.teams[team][match.teamind[team]++] = chosen2;
		match.teamSum[team]+=match.players[chosen2].leagues[guild.pickups[p].leagueid].rank.mu;
	}
	if(match.scratch.length<1){
		match.state=2;
	}
	embedMatch(message,match,true);
}

///////////////////////////
// MATCH Death
///////////////////////////

function report(guild, message, outcome){
	/* l = loss
	 * c = cancel
	 * d = draw
	 */
	[p,m] = playersMatch(guild, message.author.id);
	if(m===null){
		return u.reply(message, "You are not in a match");
	}
	if(guild.pickups[p].matches[m].state!==2 && outcome!='c'){
		u.reply(message, "Match is not awaiting result");
		return;
	}
	team = -1;
	for(let i=0;i<2;i++){
		if(guild.pickups[p].matches[m].teams[i][0] == message.author.id){
			team = i;
		}
	}
	if(team<0){
		return u.reply(message, "You are not a captain");
	}
	switch(outcome){
		case 'l':
			guild.pickups[p].matches[m].result = `${2-team}`;
			finishMatch(guild, message, p,m);
			break;
		case 'c': case 'd':
			if(guild.pickups[p].matches[m].capReport[1-team]!==outcome){
				guild.pickups[p].matches[m].capReport[team] = outcome;
				u.reply(message, "Waiting on other captain to !r"+outcome);
			}else{
				guild.pickups[p].matches[m].result = outcome;
				finishMatch(guild, message, p,m);
			}
			break;
	}
}

function finishMatch(guild, message, p, m){
	match = guild.pickups[p].matches[m];
	d.reportMatch(match);
	result = match.result;
	outMessage = `**Match ${m} ended**: \n`;
	switch(result){
		case 'c':
			outMessage = `🛑 **Match ${m} Canceled** 🛑`;
			break;
		case 'd':
			outMessage+= `⚖️ Team 1 == Team 2 ⚖️`;
			break;
		case '1':
			outMessage+= `🏆 Team 1 > Team 2 🤬`;
			break;
		case "2":
			outMessage+= `🤬 Team 1 < Team 2 🏆`;
			break;
	}

	if(match.state==2 && guild.pickups[p].settings.ranked==1) rs.rankMatch(match, message, outMessage);
	else u.send(message, outMessage, `Ended Match ${m}, result: ${match.result}`);
	for(var plr in match.players){
		delete guild.active[plr];
	}
	delete guild.pickups[p].matches[m];
}

function setMatch(message, guild, args){
	//matchid, then result (1,2,d,c)
	if(args.length<4) return u.reply(message, "Need to specify matchid and result");
	match = parseInt(args[2]);
	if(isNaN(match)) return u.reply(message, "Expected an integer matchid, but instead recieved \""+args[2]+"\"");
	result = "";
	switch(args[3].charAt(0)){
		case '1': case '2': case 'd': case 'c':
			result = args[3].charAt(0);
			break;
		default:
			return u.reply(message, "Match result must be '1', '2', 'd' or 'c'. Instead recieved \""+args[3]+"\"");
	}
	for(var p in guild.pickups){
		pck = guild.pickups[p];
		if(pck.matches.hasOwnProperty(match)){
			if(pck.matches[match].state==2 || result=='c'){
				pck.matches[match].result = result;
				return finishMatch(guild,message,p,match)
			}else{
				u.reply(message, "Cannot set result of match that is not ready for result. Instead you can only cancel match not awaiting result");
			}
		}
	}
	d.getMatch(match, function(row){
		if(row==null) return u.reply(message, "No match with id="+match+" found");
		if(!guild.pickups.hasOwnProperty(row.pickupid)){
			return u.reply("That match was not managed by this sever");
		}else{
			row.leagueids=[
				guild.pickups[row.pickupid].leagueid,
				guild.leagueid
			];
		}
		if(row.result==result) return u.reply(message, "Nothing changed (Match already had that result)");
		row.state = -1;
		row.matchid = match;
		m = new Match(row);
		if(Date.now()-m.startTime<u.parseTime("1d")){
			d.recreateRanks(m, function(players){
				tl = m.teams[0].length;
				for(let i=0;i<tl*2;i++){
					team = Math.floor(i/tl);
					p = m.teams[team][i%tl];
					m.players[p].leagues = {};
					for(var lg in players[p].leagues){
						pdata = players[p].leagues[lg];
						m.players[p].leagues[lg]={
							rank: rs.makeRank(pdata.mu,pdata.sig),
							maxrank: rs.makeRank(pdata.maxmu,pdata.maxsig),
							wins: pdata.wins,
							losses: pdata.losses,
							draws: pdata.draws,
							streak: 0 //streak history not captured
						}
						switch(m.result){
							case 'c':
								break;
							case 'd':
								m.players[p].leagues[lg].draws--;
								break;
							default:
								res = parseInt(m.result)-1;
								if(res==team) m.players[p].leagues[lg].wins--;
								else m.players[p].leagues[lg].losses--;
								break;
						}

					}
				}
				m.result = result;
				d.reportMatch(m);
				outMessage = `**Altered Match ${m.matchid}**: Team 1 `;
				switch(result){
					case 'c':
						outMessage = `**Match ${m.matchid} Undone**`;
						break;
					case 'd':
						outMessage+= `== Team 2`;
						break;
					case '1':
						outMessage+= `> Team2`;
						break;
					case "2":
						outMessage+= `< Team2`;
						break;
				};
				rs.rankMatch(m, message, outMessage);
			});
		}

	});
}

///////////////////////////////
// QUEUE
//////////////////////////////

function stringQueues(guild, message) {
	if(guild.pickups.length<1) return "No pickups created";
	outMessage = ``;
	for(var p in guild.pickups){
		pck = guild.pickups[p];
		outMessage +="**"+pck.name+"** (";
		outMessage+=pck.queue.length+"/"+pck.settings.teamsize*2+") : ";
		if(pck.queue.length==(pck.settings.teamsize*2-1)){
			outMessage+='🤏';
		}else if(pck.queue.length>=(pck.settings.teamsize*2)){
			outMessage+='🤯';
		}
		outMessage+=" [ ";
		for(let i=0;i<pck.queue.length;i++){
			outMessage+=`${guild.active[pck.queue[i]].nickname}`;
			if(i<pck.queue.length-1) outMessage+=' , ';
		}
		outMessage += ` ]\n`;
	}
	return outMessage;
}

function removeQueue(guild, message, authorId, args=[]){
	removed = false;
	for(var p in guild.pickups){
		q = guild.pickups[p].queue;
		if (q.length==0) continue;
		if(args.length>1){
			care = false;
			for(let i=0;i<args.length;i++){
				if(guild.pickups[p].name==args[i]){
					care = true;
					break;
				}
			}
			if(!care) continue; //if we don't care, go next
		}
		newQ = [];
		found=false;
		if(q.length==1){
			if(q[0]==authorId) found=true;
		}else{
			newQ = new Array(q.length-1);
			j = 0;//counter for newQueue
			for(let i=0;i<q.length;i++){
				if(q[i]==authorId) found = true;
				else if(j>newQ.length-1) break;
				else newQ[j++]=q[i];
			}
		}
		if(found){
			guild.pickups[p].queue = newQ;
			removed = true;
			if(guild.active[authorId].matchId == null) delete guild.active[authorId];
			else guild.active[authorId].queuedAt = null;
		}
	}
	if(message==null) return;
	if(removed) u.reply(message,stringQueues(guild), "Printed Queues");
	else u.reply(message, "Not in queue", "Tried to remove player not in queue");
}

////////////////////////////////
// Match
///////////////////////////////

function playersMatch(guild, id) {
	for(var p in guild.pickups){
		for(var m in guild.pickups[p].matches){
			if(guild.pickups[p].matches[m].players.hasOwnProperty(id)){
				return [p,m];
			}
		}
	}
	return [null,null];
}

function printMatches(guild, message) {
	outMessage = "Matches:";
	for(var p in guild.pickups){
		for(var m in guild.pickups[p].matches){
			outMessage+="\n"+matchString(guild.pickups[p].matches[m],true);
		}
	}
	u.reply(message, outMessage, "Printed all Matches");
}

function printMatch(guild, message, args) {
	for(var p in guild.pickups){
		for(var m in guild.pickups[p].matches){
			if(
				(args.length<2 && 
					(guild.pickups[p].matches[m].players.hasOwnProperty(message.author.id))) 
				|| (args[1]==m)
			){
				embedMatch(message, guild.pickups[p].matches[m]);
				return;
			}
		}
	}
	if(args.length>1 && !isNaN(parseInt(args[1]))){
		mid = parseInt(args[1]);
		d.getMatch(mid, function(mrow){
			if(mrow==null) {
				outMsg = "No match found for id: "+mid;
				return u.reply(message, outMsg, "!match (id not found)");
			}
			vals = {
				matchid: mid,
				pickupid: mrow.pickupid,
				state: -1,
				teams: mrow.teams,
				result: mrow.result,
				startTime: mrow.starttime,
				players: mrow.players,
				teamSum: mrow.teamSum
			};
			embedMatch(message,new Match(vals),false);
		});
	}else if(args.length>1) u.reply(message, "Expected an integer match id but instead recieved \""+args[1]+"\"", "!match (arg not an int)");
	else u.reply(message, "You are not in a match", "!match (didn't find player)");
}


function lastGame(message, args, guildid, guilds){//rename to lastMatch?
	flgVals = u.argParser(args, {
		$shorts: function(flag){
			switch(flag){
				case 'g': return "global";
				case 'r': return "region";
				case 'p': return "player";
				case 'd': return "discordid";
				case 'l': return "leagueid";
				case 's': return "season";
				default: return flag;
			}},
		global: 0,
		region: 1,
		player: 1,
		discordid: 3,
		leagueid: 2,
		season: 1,
		pickup: 1
	}, 1);
	if(flgVals.$err!='') return u.reply(message, flgVals.$err);
	if(flgVals.hasOwnProperty("region")){
		region = u.guildFromRegion(flgVals.region, guilds);
		if(region==null){
			return u.reply(message, "\""+flgVals.region+"\" is not a valid region");
		}
		flgVals.guildid = region;
	}else if(!flgVals.hasOwnProperty("global")){
		flgVals.guildid = guildid;
	}

	d.getLastgame(flgVals, function(mrow){
		if(mrow==null) {
			outMsg = "No lastgame found for";
			for(var flag in flgVals){
				if(flag.charAt(0)=='$') continue;
				outMsg +="\n"+flag+": "+flgVals[flag];
			}
			return u.reply(message, outMsg);
		}
		vals = {
			matchid: mrow.matchid,
			pickupid: mrow.pickupid,
			state: -1,
			teams: mrow.teams,
			result: mrow.result,
			startTime: mrow.starttime,
			players: mrow.players
		};
		match = new Match(vals);
		outMsg = u.toTimeString(Date.now()-mrow.starttime)+" ago.";
		outMsg+= "\n"+matchString(match, true);
		u.reply(message, outMsg);
	});
}

function matchString(match, simple=false, ping=false){
	l = match.teams[0].length;
	teamstrs = [new Array(l), new Array(l)];
	for(let i=0;i<l*2;i++){
		team = Math.floor(i/l);
		index = i%l;
		id = match.teams[team][index];
		teamstrs[team][index] = "";
		if(!simple) teamstrs[team][index] +=rs.rankGroup(match.players[id].leagues[match.leagueids[0]].rank.mu*100)+" ";
		if(ping) teamstrs[team][index] +="<@"+id+">";
		else teamstrs[team][index] += match.players[id].nickname;
	}

	outMsg = `**Match ${match.matchid}:**`;
	if(simple) {
		outMsg+=" ["+teamstrs[0].join(",")+"] ";
		switch(match.result){
			case null:
				outMsg+="vs";
				break;
			case 'd':
				outMsg+= "==";
				break;
			case 'c':
				outMsg+= "CANCELLED";
				break;
			case '1':
				outMsg+= '>';
				break;
			case '2':
				outMsg+='<';
				break;
		}
		outMsg+=" ["+teamstrs[1].join(",")+"] ";
	}else{
		outMsg+="\nTeam1:\n"+teamstrs[0].join("\n")+"\nTeam2:\n"+teamstrs[1].join("\n")+"\n";
		if(match.result!=null) outMsg+="Result: "+match.result+"\n\n";
		if(match.map) outMsg+= `Map: ${match.map}`;
	}
	return outMsg;
}

function embedMatch(message, match, ping=false){
	ping = true; //pinging does nothing in embed
	did = u.grabDid(message);
	l = match.teams[0].length;
	teamstrs = [new Array(l), new Array(l)];
	teamsize = [0,0];
	for(let i=0;i<l*2;i++){
		team = Math.floor(i/l);
		index = i%l;
		id = match.teams[team][index];
		if(id==null) continue;
		else teamsize[team]++;
		teamstrs[team][index] = '';
		if(match.players[id].hasOwnProperty("leagues")){
			teamstrs[team][index]+=rs.rankGroup(match.players[id].leagues[match.leagueids[0]].rank.mu*100)+" ";
		}
		if(ping) teamstrs[team][index] +="<@"+id+">";
		else teamstrs[team][index] +=match.players[id].nickname;
	}
	tFieldNames = ["Team 1","Team 2"];
	for(let i=0;i<2;i++){
		if(match.teamSum[i]!=0) tFieldNames[i]+=" ("+rs.stringMu(match.teamSum[i]/teamsize[i])+")";
	}
	outEmb = new MessageEmbed()
		.setTitle("Match "+match.matchid)
		.addFields(
			{name:tFieldNames[0], value:teamstrs[0].join("\n"), inline:true},
			{name:tFieldNames[1], value:teamstrs[1].join("\n"), inline:true}
		);
	if(match.map) outEmb.addFields({name:"Map", value:match.map, inline:false});
	if(match.scratch!=null && match.scratch.length>0){
		ind = (l-1)*2-match.scratch.length;
		team = match.pickorder[ind];
		//whopick = match.teams[team][0];
		whopick = match.players[match.teams[team][0]].nickname;
		listPicks = "";
		for(let i=0;i<match.scratch.length;i++){
			listPicks+=rs.rankGroup(match.players[match.scratch[i]].leagues[match.leagueids[0]].rank.mu*100);
			//listPicks+=match.players[match.scratch[i]].nickname+"\n";
			listPicks+="<@"+match.scratch[i]+">\n";
		}
		outEmb.addFields({
			name:""+whopick+"'s turn to pick:",
			value:listPicks,
			inline:false
		});
	}
	if(match.pickteams!=null){
		outEmb.addFields({
			name: "Pickteams Algorithmn:",
			value: match.pickteams,
			inline: true
		});
	}
	if(match.result!=null){
		result = "";
		switch(match.result){
			case 'd':
				result = "Draw";
				break;
			case 'c':
				result = "CANCELLED";
				break;
			case '1':
				result = "Team 1 Won";
				break;
			case '2':
				result = "Team 2 Won";
				break;
		}
		outEmb.addFields({name:"Result", value:result, inline:true});
	}
	message.channel.send({embeds: [outEmb]})
		.then(function(){
			console.log(""+did+" Embd: Match "+match.matchid);
		});
}

module.exports = {
	addQueue,
	stringQueues,
	removeQueue,
	printMatches,
	printMatch,
	report,
	lastGame,
	setMatch,
	pick,
	requireReady,
	createMatch,
	embedMatch,
	readyFail
};
