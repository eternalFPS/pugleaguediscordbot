const d = require("./dbreqs.js");
const u = require("./utils.js");
const mm = require("./matchmaker.js");
const cfg = require("./config.json");

///////////////////////////////////
//  Configuration
///////////////////////////////////

function set(message, guild, args){
	if(args.length<3) return u.reply(message, "Expected more arguments", "!set [fail:need more args]");
	switch(args[1]){
		case "-u": case "--user":
			setUser(message, args);
			break;
		case "-d": case "--discord": case "-g": case "--guild": case "-s": case "--server":
			if(hasPerms(message, guild, 100)){
				setGuild(message, guild, args);
			}else{
				return u.reply(message, "Must be an administrator to modify guild settings", "!set -g [fail:not admin]");
			}
			break;
		case "-p": case "--pickup":
			if(hasPerms(message, guild, 100)){
				setPickup(message, guild, args);
			}else{
				return u.reply(message, "Must be an administrator to modify pickup settings", "!set -p [fail:not admin]");
			}
			break;
		case "-m": case "--match":
			if(hasPerms(message, guild, 10)){
				mm.setMatch(message, guild, args);
			}else{
				return u.reply(message, "Must be a Moderator or higher to set match results", "!set -m [fail:not mod]");
			}
			break;
		default:
			return u.reply(message, "\""+args[1]+"\" is not valid for specifying what object you are trying to set. You can set settings for yourself (-u) for this server (-s) for a pickup (-p) or for a match (-m)", "!set [fail:invalid flag]");
	}
}

function setUser(message, args){
	noValue = (args.length<4 || args[3].toLowerCase()=="none" || args[3].toLowerCase()=='""' || args[3].toLowerCase()=="-");
	switch(args[2]){
		case "country": case "nation":
			if(noValue){
				d.setUser(message.author.id, "country", null);
				return u.reply(message, "Removed your country setting", "!set -u country");
			}
			iso = args[3].toUpperCase();
			if(iso.length!=2){
				if(cfg.flags.toIso.hasOwnProperty(iso)){
					iso = cfg.flags.toIso[iso];
				}else{
					return u.reply(message, "Did not recognise that country. Try the ISO code instead", "!set -u country [fail:unrecognised");
				}
			}else if(!cfg.flags.toEmoji.hasOwnProperty(iso)){
				return u.reply(message, "Not a valid ISO code.", "!set -u country [fail:unrecognised");
			}
			d.setUser(message.author.id, "country", iso);
			u.reply(message, "Set country to "+cfg.flags.toEmoji[iso], "!set -u country");
			break;
		case "class":
			if(noValue){
				d.setUser(message.author.id, "class", 0);
				return u.reply(message, "Set your class to Flex", "!set -u class");
			}
			sClass = -1;
			for(let i=0;i<cfg.class.length;i++){
				if(args[3].toLowerCase()==cfg.class[i].toLowerCase()){
					sClass = i;
					break;
				}else if(args[3].toLowerCase()==cfg.classsimple[i].toLowerCase()){
					sClass = i;
					break;
				}
			}
			if(sClass==-1){
				return u.reply(message, "Not a valid class","!set -u class [fail:invalid]");
			}
			d.setUser(message.author.id, "class", sClass);
			u.reply(message, "Set class to "+args[3]);
			break;
		case "team": case "clan":
			if(noValue){
				d.setUser(message.author.id, "team", null);
				return u.reply(message, "Removed your team");
			}
			if(args[3].length>4){
				return u.reply(message, ""+args[2]+" must be no longer than 4 characters (and its currently "+args[3].length+" characters)");
			}
			d.setUser(message.author.id, "team", args[3]);
			u.reply(message, "Set "+args[2]+" to \""+args[3]+"\"");
			break;
		default:
			u.reply(message, "Not a valid setting");
			break;
	}
}

function setGuild(message, guild, args){
	value = '';
	if(
		args.length<4 || 
		args[3].toLowerCase()=="none" || 
		args[3].toLowerCase()=='""' || 
		args[3].toLowerCase()=="-"
	) value = null;
	setting = args[2].toLowerCase();
	// AHHHH I LOVE SWITCHES WHY ARE THEY SO USEFUL
	// some settings need values + stopping settings that don't exist
	switch(setting){
		case "overlordid": case "name":
			if(value==null) return u.reply(message, "expected a value for "+setting+" but didn't recieve one");
			break;
		case "mainpickupid": case "adminrole": case "modrole": case "readytime": case "noaddrole": case "matchchannel": case "promotedelay": case "promoterole": case "placementgames":
			break;
		default:
			return u.reply(message, "\""+setting+"\" is not a valid guild setting");
	}
	//value parsing
	if(value==''){
		switch(setting){
			// we want a discord tag
			case "overlordid":
			mention = u.idFromMention(args[3]);
			if(mention=='') return u.reply(message, "Expected a user tag but recieved \""+args[3]+"\" instead");
			// int but as a string
			case "adminrole": case "modrole": case "noaddrole": case "promoterole":
				toInt = parseInt(args[3]);
				if(isNaN(toInt)){
					return u.reply(message, "Expected a discordid but recieved \""+args[3]+"\" instead");
				}else value=args[3];
				break;
			// int as an int
			case "mainpickupid":
				toInt = parseInt(args[3]);
				if(isNaN(toInt)){
					return u.reply(message, "Expected an integer value but recieved \""+args[3]+"\" instead");
				}else value=toInt;
				break
			//int > 0
			case "placementgames":
				toInt = parseInt(args[3]);
				if(isNaN(toInt) || toInt<=0){
					return u.reply(message, "Expected an integer >0 but recieved \""+args[3]+"\" instead");
				}else value=toInt;
				break
			//time
			case "readytime": case "promotedelay":
				time = u.parseTime(args[3]);
				if(time==-1){
					return u.reply(message, "Expected a time but recieved \""+time+"\" instead");
				}
				value = time;
				break;
			// channels (as id or tag)
			case "matchchannel":
				if(args[3].startsWith("<#") && args[3].endsWith(">")){
					value = args[3].slice(2,-1);
				}else if(!isNaN(parseInt(args[3]))){
					value = args[3];
				}else{
					return u.reply(message, "Expected a channel mention or id but instead recieved \""+args[3]+"\"");
				}
				break;
			// string = default
			default:
				value = args[3];
				break;
		}
	}
	if(setting=="overlordid" || setting=="name"){
		guild[setting] = value;
	}else{
		guild.settings[setting] = value;
	}
	if(setting=="placementgames") d.setPlacementgames(guild.leagueid, value);
	d.setGuild(guild.guildid, setting, value);
	u.reply(message, "set **"+setting+"** to \""+value+"\"");
}

function setPickup(message, guild, args){
	if(args.length<3){
		return u.reply(message, "expected more arguments (did you specify which pickup?)");
	}else if(args.length<4){
		return u.reply(message, "expected more arguments (did you specify which setting?)");
	}
	pickup = -1;//-1 meams we are gonna change all pickups
	if(args[2]!='*'){
		for(var p in guild.pickups){
			if(guild.pickups[p].name==args[2]){
				pickup = p;
				break;
			}
		}
		if(pickup==-1) return u.reply(message, "Did not find pickup \""+args[2]+"\"");
	}
	value = '';
	if(
		args.length<5 || 
		args[4].toLowerCase()=="none" || 
		args[4].toLowerCase()=='""' || 
		args[4].toLowerCase()=="-"
	) value = null;
	setting = args[3].toLowerCase();
	// some settings need values + stopping settings that don't exist
	switch(setting){
		case "name": case "teamsize":
			if(value==null) return u.reply(message, "expected a value for "+setting+" but didn't recieve one");
			break;
		case "mappool": case "whiterole": case "blackrole": case "pickteams": case "ranked": case "teamsize": case "placementgames":
			break;
		default:
			return u.reply(message, "\""+setting+"\" is not a valid guild setting");
	}
	//value parsing
	if(value==''){
		switch(setting){
			// int but as a string
			case "whiterole": case "blackrole":
				toInt = parseInt(args[4]);
				if(isNaN(toInt)){
					return u.reply(message, "Expected a discordid but recieved \""+args[3]+"\" instead");
				}else value=args[4];
				break;
			// int >0
			case "teamsize": case "placementgames":
				toInt = parseInt(args[4]);
				if(isNaN(toInt)){
					return u.reply(message, "Expected an integer value but recieved \""+args[4]+"\" instead");
				}else if(toInt<=0){
					return u.reply(message, "Teamsize must be greater than 0");
				}else value=toInt;
				break;
			// mappool as csv or arg array
			case "mappool":
				if(args.length>5){
					value = args.slice(4).join(',');
				}else{
					value = args[4];
				}
				break;
			// pickteams set values
			case "pickteams":
				switch(args[4]){
					case "auto": case "autos": case "autop": case "autoi": case "manual": case "manualo": case "exp": case "auto-s": case "auto-p": case "auto-i": case "manual-o": case "experimental":
						value = args[4];
						break;
					default:
						return u.reply(message, "\""+args[4]+"\" is not a valid value for pickteams setting");
				}
				break;
			// 1 or 0
			case "ranked":
				toInt = parseInt(args[4]);
				if(isNaN(toInt)){
					return u.reply(message, "Expected an integer value but recieved \""+args[4]+"\" instead");
				}else if(toInt!=0 && toInt!=1){
					return u.reply(message, "Ranked value must be 0 or 1");
				}else value=toInt;
				break;
			// string = default
			// also for name
			default:
				value = args[4];
				break;
		}
	}
	for(var p in guild.pickups){
		if(pickup==-1 || pickup==p){
			if(setting=="name"){
				guild.pickups[p][setting] = value;
			}else if(setting=="mappool"){
				guild.pickups[p].settings[setting] = value.split(',');
			}else{
				guild.pickups[p].settings[setting] = value;
			}
			if(setting=="placementgames") d.setPlacementgames(guild.pickups[p].leagueid, value);
			d.setPickup(p,setting,value);
		}
	}
	outMsg = "set **"+setting+"** to \""+value+"\" ";
	if(pickup==-1) outMsg +="in all pickups";
	else outMsg +="in pickup "+args[2];
	u.reply(message, outMsg);
}


function printConfig(message, args, guilds){
	flgVals = u.argParser(args, {
		$shorts: function(flag){
			switch(flag){
				case 'p': return "pickup";
				case 'r': return "region";
				default: return flag;
			}},
		pickup: 0,
		region: 1
	}, 1);
	if(flgVals.$err!='') return u.reply(message, flgVals.$err, "!stats (flag parse error)");
	guildid = message.guildId;
	if(flgVals.hasOwnProperty("region")){
		guildid = u.guildFromRegion(flgVals.region, guilds);
		if(guildid==null) return u.reply(message, "Not a valid region");
	}
	guild = guilds[guildid];
	outMessage = "Config:\n```";
	if(flgVals.hasOwnProperty("pickup") || flgVals.$free.length>0){
		p = null;
		if(flgVals.$free.length>0) {
			for(var pck in guild.pickups){
				if(guild.pickups[pck].name==flgVals.$free[0]) {
					p=pck;
					break;
				}
			}
			if(p==null){
				return u.reply(message, "Did not find that pickup");
			}
		}else{
			p = guild.settings.mainpickupid;
		}
		for (var key in guild.pickups[p]) {
			switch(key){
				case "settings":
					for(var setting in guild.pickups[p].settings){
						outMessage+="\n"+setting+": "+guild.pickups[p].settings[setting];
					}
					break;
				case "matches": case "queue":
					continue;
				default:
					outMessage+=""+key+": "+guild.pickups[p][key]+"\n";
					break;
			}
		}
	}else{
		for (var key in guild) {
			switch(key){
				case "settings":
					for(var setting in guild.settings){
						outMessage+="\n"+setting+": "+guild.settings[setting];
					}
					break;
				case "active": case "pickups": case "lastPromote": case "waitingReact":
					continue;
				default:
					outMessage+=""+key+": "+guild[key]+"\n";
					break;
			}
		}

	}
	u.dm(message, outMessage+"```", "Printed all Configs");
}

//////////////////////////////////////////
// Stats and Info
/////////////////////////////////////////

function printStats(message, args, guilds){
	guildid = message.guildId;
	flgVals = {
		guildid: guildid
	};
	if(args.length>1){
		flgVals = u.argParser(args,{
			$shorts: function(flag){
				switch(flag){
					case 'p': return "pickup";
					case 'r': return "region";
					case 'g': return "global";
					case 'l': return "leagueid";
					case 's': return "season";
				}
			},
			pickup: 1,
			region: 1,
			global: 0,
			leagueid: 2,
			season: 1
		}, 1);
		if(flgVals.$err!='') return u.reply(message, flgVals.$err, "!stats (flag parse error)");
		if(flgVals.hasOwnProperty("region")){
			region = u.guildFromRegion(flgVals.region, guilds);
			if(region==null){
				return u.reply(message, "♿ \""+flgVals.region+"\" is not a valid region ♿", "!stats (not a valid region)");
			}
			flgVals.guildid = region;
		}else if(!flgVals.hasOwnProperty("global")){
			flgVals.guildid = guildid;
		}
	}
	d.totalStats(flgVals, function(rows){
		draws = 0;
		totalgames = 0;
		for(let i=0;i<rows.length;i++){
			totalgames += rows[i]['COUNT(*)'];
			if(rows[i]["matches.result='d'"]==1) draws += rows[i]['COUNT(*)'];
		}
		outMsg = "";
		if(totalgames>0){
			outMsg += "**🧮 Totalgames:** "+totalgames+"\n**⚖️ Draws:** "+draws+" ("+Math.round(10000*draws/totalgames)/100+"% drawrate)";
		}else{
			outMsg+= "**😢 No games recorded for**";
			for(var flg in flgVals){
				if(flg=="$err"||flg=="$free") continue;
				outMsg+="\n**"+flg+":** "+flgVals[flg];
			}
		}
		u.reply(message, outMsg, "!stats");
	});
}

function top(message, args, guilds, guildid){
	time = 604800000; // 1 week
	flgVals = u.argParser(args, {
		$shorts: function(flag){
			switch(flag){
				case 'd': return "discordids"; //prints discordids
				case 'g': return "global";
				case 'r': return "region";
				case 'a': return "alltime";
				case 'p': return "pickup";
				case 'l': return "leagueid";
				case 't': return "time";
				case 's': return "season";
				default: return flag;
			}
		},
		discordids: 0,
		global: 0,
		region: 1,
		alltime: 0,
		page: 2,
		leagueid: 2,
		pickup: 1,
		time: 1,
		season: 1,
	}, 1);
	if(flgVals.$err!='') return u.reply(message, flgVals.$err);
	if(!flgVals.hasOwnProperty("alltime")){
		if(!flgVals.hasOwnProperty("time")){
			if(flgVals.$free.length>0) time = u.parseTime(flgVals.$free[0]);
		}else{
			time = u.parseTime(flgVals.time);
		}
		if(time<0) return u.reply(message, "Not a valid period of time. Expected something like \"!top w\" or \"!top 5d\"");
		else flgVals.time = Date.now()-time;
	}else{
		delete flgVals.time;
	}
	if(flgVals.hasOwnProperty("region")){
		region = u.guildFromRegion(flgVals.region, guilds);
		if(region==null){
			return u.reply(message, "\""+flgVals.region+"\" is not a valid region");
		}
		flgVals.guildid = region;
	}else if(!flgVals.hasOwnProperty("global")) {
		flgVals.guildid = guildid;
	}
	if(flgVals.hasOwnProperty("page") && flgVals.page==0) flgVals.page=1;
	d.getTop(flgVals, function(rows){
		if(rows.length<1) return u.reply(message, "No matches");
		outMsg = "**Top**\n";
		for(let i=0;i<rows.length;i++) {
			userRef = "";
			if(flgVals.hasOwnProperty("discordids")){
				userRef = rows[i].discordid;
			}else userRef = rows[i].nickname;
			pos = i+1;
			if(flgVals.hasOwnProperty("page")) pos+=(flgVals-1)*10;
			outMsg+=""+pos+". "+userRef+" ("+rows[i]["COUNT(*)"]+")\n";
		}
		u.reply(message, outMsg);
	});
}

function printMaps(message, args, guilds){
	guildid = message.guildId;
	if(args.length>1){
		guildid = u.guildFromRegion(args[1], guilds);
		if(guildid==null){
			return u.reply(message, "\""+args[1]+"\" is not a valid region");
		}
	}
	outMsg = "";
	if(guilds[guildid].name=="") outMsg+="**Guild: "+guildid+"**";
	else outMsg+="**Region: "+guilds[guildid].name+"**";
	for(var p in guilds[guildid].pickups){
		pickup = guilds[guildid].pickups[p];
		outMsg+="\n**"+pickup.name+"**: "+pickup.settings.mappool;
	}
	u.reply(message, outMsg);
}

function promote(message,guild){
	outMsg = "";
	promotedelay = guild.settings.promotedelay;
	timeleft = promotedelay - (Date.now()-guild.lastPromote);
	if(timeleft<0){
		needed = 999;
		for(var pickup in guild.pickups){
			p = guild.pickups[pickup];
			tofill = p.settings.teamsize*2 - p.queue.length;
			if(tofill<needed) needed = tofill;
		}
		outMsg = "Need "+needed+" more players for pickups!";
		guild.lastPromote = Date.now();
		promoterole = guild.settings.promoterole;
		if(promoterole!=null) outMsg +=" <@&"+promoterole+">";
	}else outMsg = "Need to wait another "+u.toTimeString(timeleft)+" before promoting";
	u.send(message, outMsg, "!promote", ["roles"]);
}


////////////////////////////////////
// Noadd / Bans / Infractions
/////////////////////////////////////

function noadd(message, args, guild){
	if(!hasPerms(message, guild, 10)){
		return u.reply(message, "Must be a moderator or higher to noadd players");
	}
	if(args.length<2){
		u.reply(message, "No player specified");
		return;
	}else{
		mention = u.idFromMention(args[1]);
		if(mention=='') return u.reply(message, "Expected a player **mention** but recieved \""+args[1]+"\" instead");
		//our 3rd argument (after command and player)
		//could be one of 3 things:
		//1. Time
		//2. Reason
		//3. Permanent flag
		time = null;
		reason = "";
		for(let i=2;i<args.length;i++){
			pT = u.parseTime(args[i]);
			if(pT>-1) {
				time = pT;
			}else if(args[i]=="-p" || args[i]=="--permanent"){
				time = -1;
			}else reason+=args[i]+" ";
		}
		d.addInfraction(mention,guild.guildid,time,reason,message.author.id,function(insert,newVals){
			outMsg = "";
			if(insert) outMsg = "**Noadded Discordid:** "+mention;
			else outMsg = "**Updated Noadd for Discordid:** "+mention;
			if(newVals.$duration==-1) outMsg+="\n**Permanent Ban**\n";
			else outMsg+="\n**Expires in:** "+u.toTimeString(newVals.$duration+newVals.$issuetime-Date.now())+"\n";
			outMsg +="**Reason:** "+newVals.$reason;
			u.reply(message, outMsg);
		});
	}
}

function forgive(message, args, guild){
	if(!hasPerms(message, guild, 10)){
		return u.reply(message, "Must be a moderator or higher to forgive players");
	}
	if(args.length<2) {
		return u.reply(message, "No player specified");
	}else{
		mention = u.idFromMention(args[1]);
		if(mention=='') return u.reply(message, "Expected a player **mention** but recieved \""+args[1]+"\" instead");
		d.forgiveInfraction(mention, guild.guildid, message.author, function(){

			u.reply(message, "**Forgave all current noadds for Discordid: **"+mention);
		});
	}
}

function listNoadds(message, guilds,args){
	flgVals = {};
	if(args>1){
		flgVals = u.argParser(args, {
			$shorts: function(flag){
				switch(flag){
					case 'p': return "player";
					case 'd': return "discordid";
					case 'h': return "historic";
					case 'g': return "global";
					case 'r': return "region";
					case 's': return "simple";
					default: return flag;
				}
			},
			player: 1,
			discordid: 3,
			historic: 0,
			global: 0,
			region: 1,
			simple: 0
		}, 1);
		if(flgVals.$err!='') return u.reply(message, flgVals.$err);
		if(flgVals.hasOwnProperty("region")){
			flgVals.guildid = u.guildFromRegion(flgVals.region, guilds)
		}else if(!flgVals.hasOwnProperty("global")){
			flgVals.guildid = message.guildId;
		}
		for(let i=0;i<flgVals.$free.length;i++){
			number = parseInt(flgVals.$free[i]);
			if(isNaN(number)) flgVals.player = flgVals.$free[i];
			else flgVals.page = number;
		}
	}else{
		flgVals.guildid = message.guildId;
	}
	if(!flgVals.hasOwnProperty("page") || flgVals.page==0){
		flgVals.page = 1;
	}
	d.getInfractions(flgVals, function(rows) {
		u.reply(message, printNoadds(rows, flgVals.page), `Printed Noadds`);
	});
}

function printNoadds(rows, number){
	if(rows.length==0){
		if(number==1) return "No noadds";
		else return "Not that many pages";
	}
	outMsg = "```markdown\nDDMMMYY | Nickname(id) | Expiry/Duration | Reason (Issuer)\n------------------------------------------------\n";
	for(let i=0;i<rows.length;i++){
		id = new Date(rows[i].issuetime);
		outMsg+=u.dateString(id)+" | ";
		outMsg+=rows[i].nickname+"("+rows[i].discordid+") | ";

		howLong = '';
		if(rows[i].duration<0) howLong = "PERMANENT";
		else howLong = u.toTimeString(rows[i].duration);

		if(rows[i].forgiven!=null) outMsg+="Forgiven ("+howLong+")";
		else{
			exp = (rows[i].issuetime+rows[i].duration)-Date.now();
			if(exp<0) outMsg+="Expired ("+howLong+")";
			else outMsg+=u.toTimeString(exp)+" Left of "+howLong;
		}
		outMsg+=" | "+rows[i].reason+" ("+rows[i].issuer+")";
		if(rows[i].forgiven!=null) outMsg+=" Forgiver: "+rows[i].forgiven;
		outMsg+="\n";
	}
	outMsg+="```";
	return outMsg;
}

////////////////////////////
// Permissions
///////////////////////////

function hasPerms(message, guild, level){
	/*LEVEL:
	 * OVERLORD == 1,000
	 * Admin	==   100
	 * Moderator==    10
	 * Player   ==     0 (unused)
	 */
	if(level>1000) {
		throw new Error("Requesting permissions higher than overlord");
		return false;
	}else if(message.author.id==guild.overlordid) return true;
	else if(level>100) return false;
	else if(guild.settings.adminrole!=null && message.member.roles.cache.has(guild.settings.adminrole)) return true;
	else if(level>10) return false;
	else if(guild.settings.modrole!=null && message.member.roles.cache.has(guild.settings.modrole)) return true;
	else if(level>0) return false;
	else {
		throw new Error("Requesting permissions below that of a player");
		return true;
	}

}

module.exports = {
	set,
	printConfig,
	listNoadds,
	printStats,
	printMaps,
	noadd,
	top,
	promote,
	forgive,
	hasPerms
};
