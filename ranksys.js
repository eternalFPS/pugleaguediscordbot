const {MessageEmbed} = require('discord.js');
const r = require('openskill');
const d = require('./dbreqs.js');
const u = require("./utils.js");
const cfg = require("./config.json");

///////////////////////////////
// Querying
/////////////////////////////

function getLeague(authorId, leagueId, cb=null){
	d.getRank(authorId, leagueId, function(row) {
		league = {};
		if(typeof row === 'undefined'){
			league.rank = new r.rating();
			league.maxrank = new r.rating({mu:-50,sigma:25/8});
			league.wins = 0;
			league.losses = 0;
			league.draws = 0;
			league.streak = 0;
		}else{
			league.rank = new r.rating({mu:row.mu,sigma:row.sig});
			league.maxrank = new r.rating({mu:row.maxmu,sigma:row.maxsig});
			league.wins = row.wins;
			league.losses = row.losses;
			league.draws = row.draws;
			league.streak = row.streak;
		}
		cb(league);
	});
}

function getRank(message, args, guilds){
	guildid = message.guildId;
	flgVals = {};
	if(args.length>1){
		flgVals = u.argParser(args,{
			$shorts: function(flag){
				switch(flag){
					case 'm': return "max";
					case 'c': return "current";
					case 'p': return "player";
					case 'g': return "global";
					case 'a': return "alltime";
					case 'r': return "region";
					case 'd': return "discordid";
					case 'l': return "leagueid";
					case 's': return "season";
				}},
			max: 0,
			current: 0,
			player: 1,
			global: 0,
			alltime: 0,
			region: 1,
			discordid: 3,
			leagueid: 2,
			season: 1
		}, 1);
		if(flgVals.$err!='') {
			return u.reply(message, flgVals.$err, "!rank (failed to parse args)");
		}
		if(flgVals.$free.length>0 && !flgVals.hasOwnProperty("player") && !flgVals.hasOwnProperty("discordid")) {
			flgVals.player = flgVals.$free[0];
		}
		if(flgVals.hasOwnProperty("alltime")) {
			flgVals.guildid=guildid;
		}
		if(flgVals.hasOwnProperty("region")){
			region = u.guildFromRegion(flgVals.region, guilds);
			if(region==null){
				return u.reply(message, "\""+flgVals.region+"\" is not a valid region", "!rank (not a valid region)");
			}
			flgVals.guildid = region;
		}
		if(flgVals.hasOwnProperty("season") && !flgVals.hasOwnProperty("guildid")){
			flgVals.guildid=guildid;
		}
	}
	if(!flgVals.hasOwnProperty("discordid") && !flgVals.hasOwnProperty("player")){
		flgVals.discordid = message.author.id;
	}
	if(!flgVals.hasOwnProperty("global") &&
		!flgVals.hasOwnProperty("guildid") &&
		!flgVals.hasOwnProperty("leagueid")){
		pickups = guilds[guildid].pickups;
		mainpickupid = guilds[guildid].settings.mainpickupid;
		if(mainpickupid==null) return u.reply(message, "Pickups not setup on this server", "!rank (pickups not set up on this server)");
		flgVals.leagueid = pickups[mainpickupid].leagueid;
	}

	d.getDetailedRank(flgVals, function(row){
		if(row==null){
			outMsg="Nothing found with these options:";
			for(var opt in flgVals){
				if(opt=="$err"||opt=="$free") continue;
				outMsg+="\n"+opt+": "+flgVals[opt];
			}
			return u.reply(message, outMsg);
		}
		desc = "";
		totalgames = row.wins+row.losses+row.draws;
		maxrank = "";
		if(totalgames>=row.placementgames) {
			desc = `${stringRank({mu:row.mu, sigma:row.sig})}`;
			maxrank = stringRank({mu:row.maxmu,sigma:row.maxsig});
		}else{
			desc = `${totalgames}/${row.placementgames} Placement Games`;
			maxrank = "Unplaced";
		}
		streakFire = '';
		for(let i=3;i<row.streak;i+=3){
			streakFire+='🔥';
		}
		const outEmbed = new MessageEmbed()
			//.setColor('#0099ff')
			.setTitle(row.nickname)
			.setDescription(desc)
			.addFields(
				{ name: 'Position', value:"#"+row.pos, inline: true},
				{ name: 'W/L/D (Win%)', value: `${row.wins}/${row.losses}/${row.draws} (${Math.round(100*(row.wins/(row.wins+row.losses)))}%)`, inline:true},
				{name: "Max Rank", value: maxrank, inline:true},
				{ name: 'Matches', value: ""+totalgames, inline: true },
				{ name: 'Streak', value: ""+row.streak+streakFire, inline: true },
				{name: "Last 10", value: "unimplemented", inline:true}
			)
			.setTimestamp();
		if(row.country!=null && row.country.length>0){
			outEmbed.addFields({
				name: "Country",
				value: u.cntToEmoji(row.country),
				inline:true
			});
		}
		outEmbed.addFields({
			name: "Class",
			value: u.idToClass(row.class),
			inline:true
		});
		if(row.team!=null && row.team.length>0){
			outEmbed.addFields({
				name: "Team",
				value: row.team,
				inline:true
			});
		}

		message.channel.send({ embeds: [outEmbed] });
		console.log("outputted embed");

	});
}


function getLB(message, args, guilds){
	guildid = message.guildId;
	flgVals = {};
	flgVals = u.argParser(args,{
		$shorts: function(flag){
			switch(flag){
				case 'm': return "max";
				case 'c': return "current";
				case 'p': return "page";
				case 'g': return "global";
				case 'a': return "alltime";
				case 'r': return "region";
				case 'l': return "leagueid";
				case 's': return "season";
			}},
		max: 0,
		current: 0,
		page: 2,
		global: 0,
		alltime: 0,
		region: 1,
		leagueid: 2,
		season: 1
	}, 1);
	if(flgVals.$err!='') {
		return u.reply(message, flgVals.$err, "!lb (failed to parse args)");
	}
	if(flgVals.$free.length>0 && !flgVals.hasOwnProperty("page")) {
		pg = parseInt(flgVals.$free[0]);
		if(isNaN(pg)){
			return u.reply(message, "expected a page number but instead recieved \""+flgVals.$free[0]+"\"");
		}
		flgVals.page = pg;
	}
	if(flgVals.hasOwnProperty("alltime")) {
		flgVals.guildid=guildid;
	}
	if(flgVals.hasOwnProperty("region")){
		region = u.guildFromRegion(flgVals.region, guilds);
		if(region==null){
			return u.reply(message, "\""+flgVals.region+"\" is not a valid region", "!rank (not a valid region)");
		}
		flgVals.guildid = region;
	}
	if(flgVals.hasOwnProperty("season") && !flgVals.hasOwnProperty("guildid")){
		flgVals.guildid=guildid;
	}
	if(!flgVals.hasOwnProperty("page") || flgVals.page<1){
		flgVals.page = 1;
	}
	if(!flgVals.hasOwnProperty("global") &&
		!flgVals.hasOwnProperty("guildid") &&
		!flgVals.hasOwnProperty("leagueid")){
		pickups = guilds[guildid].pickups;
		mainpickupid = guilds[guildid].settings.mainpickupid;
		if(mainpickupid==null) return u.reply(message, "Pickups not setup on this server", "!lb (pickups not set up on this server)");
		flgVals.leagueid = pickups[mainpickupid].leagueid;
	}

	d.getLB(flgVals, function(rows){
		u.reply(message, printLB(rows, flgVals.page), "Printed LB");
	});

}

function ranksTable(message, args, guilds, guildId){
	outMsg = "**Ranks:**\n";
	rg = cfg.rankGroups;
	for(let i=0;i<rg.values.length;i++){
		outMsg+=u.fitString(">= "+rg.values[i],7,-1)+" ["+rg.icons[i]+"]\n";
		if(i==rg.values.length-1){
			outMsg+=u.fitString(" < "+rg.values[i],7,-1)+" ["+rg.icons[i+1]+"]";
		}
	}
	u.reply(message, outMsg);	
}

function makeRank(mu, sig){
	return new r.rating({mu:mu,sigma:sig});
}

///////////////////////////////
// Ranking
///////////////////////

function rankMatch(match, message, outMsg){
	outMsg += "\n";
	l = match.teams[0].length;
	for(let x=0;x<match.leagueids.length;x++){
		lg = match.leagueids[x];
		d.getPlacementGames(lg,function(row){
			placementgames = row.placementgames;
			lid = row.leagueid;//because lg gets overwritten due to sync
			ranks = [new Array(l),new Array(l)];
			//we need to reconstruct the ranks as an array for openskill to process it.
			//TODO fix up openskill code so it can process objects, not just arrays
			//and while we are at it, also fix the score = [0,0] bug.
			for(let tm = 0;tm<2;tm++){
				for(let i=0;i<l;i++){
					pl = match.teams[tm][i];
					//pllg = player data for league lg
					pllg = match.players[pl].leagues[lid];
					ranks[tm][i] = pllg.rank; 
				}
			}
			newranks = [[],[]];
			score = [0,0];
			if(match.result=='d') {
				newranks = r.rate(ranks, {score:[1,1]});//bug with openskill
			}else if(match.result=='c'){
				newranks = ranks;
			}else{
				winner = parseInt(match.result);
				if(isNaN(winner)) throw new Error("Expected 1 or 2 as match result but instead recieved \""+match.result+"\"");
				score[winner-1]=1;
				newranks = r.rate(ranks, {score:score});
			}
			for(let tm=0;tm<2;tm++){
				if(x==0) outMsg +="\n[Team "+(tm+1)+"]\n";
				for(let i=0;i<l;i++){
					pl = match.teams[tm][i];
					pllg = match.players[pl].leagues[lid];
					oldrank = pllg.rank;
					newrank = newranks[tm][i];

					wins = pllg.wins+score[tm];
					losses = pllg.losses+score[1-tm];
					draws = pllg.draws;
					streak = pllg.streak;
					if(match.result=='d') draws++;
					if(streak>0&&score[tm]==1){
						streak++;
					}else if(streak<0&&score[1-tm]==1){
						streak--;
					}else if(match.result!='c'){
						streak = score[tm]-score[1-tm];
					}
					data = {
						discordid: pl,
						leagueid: lid,
						mu: newrank.mu,
						sig: newrank.sigma,
						maxmu: pllg.maxrank.mu,
						maxsig: pllg.maxrank.sigma,
						wins: wins,
						losses: losses,
						draws: draws,
						matchid: match.matchid,
						muchange: newrank.mu - oldrank.mu,
						sigchange: newrank.sigma - oldrank.sigma,
						class: null,
						streak: streak
					};
					placed = Math.sign(wins+losses+draws-placementgames);
					if(placed>=0 && newrank.mu > pllg.maxrank.mu){
						data.maxmu = newrank.mu;
						data.maxsig = newrank.sigma;
					}
					d.updateRank(data);
					if(x==0){ //first league is the one displayed
						//outMsg +=`**${match.players[pl].nickname}**: `;
						outMsg +="<@"+pl+">: ";
						outMsg +=stringUpdateRank(oldrank, newrank, placed);
						if(placed<0) outMsg+=" ["+(wins+draws+losses)+"/"+placementgames+"]";
						outMsg+='\n';
					}
				}
			}
			if(x==0) u.send(message, outMsg, `Ended Match ${m}, result: ${match.result}`, ["users"]);
		});
	}
}

////////////////////////////
// Strings
////////////////////////////

function printLB(rows, page){
	if(rows==null || rows.length<1) return "Nothing Found";
	outMessage = "```markdown\n";
	outMessage+= "  # | CT |   Rating   |         Name         | Match | W/L (Win%)\n";
	outMessage+= "=======================================================================\n";
	for(let i=0;i<rows.length;i++){
		tgames = rows[i].wins+rows[i].losses+rows[i].draws;
		pos = (page-1)*10+i+1;
		country = rows[i].country;
		if(country==null || country=='') country = "  ";
		outMessage+=u.fitString(pos,3,1)+" |";
		outMessage+=" "+country+" |";
		rank = {};
		if(rows[i].hasOwnProperty("mu")){
			rank.mu = rows[i].mu;
			rank.sig = rows[i].sig;
		}else if(rows[i].hasOwnProperty("maxmu")){
			rank.mu = rows[i].maxmu;
			rank.sig = rows[i].maxsig;
		}else throw new Error("Printing LB but not recieving ranks");
		outMessage+=" "+u.fitString(stringRank(rank),11,-1)+"|";
		outMessage+=u.fitString(rows[i].nickname,22)+'|';
		outMessage+=" "+u.fitString(tgames,5,1)+" |";
		outMessage+=" "+rows[i].wins+'/'+rows[i].losses+" ("+Math.round(100*rows[i].wins/(rows[i].wins+rows[i].losses))+"%)\n";
	}
	outMessage+="```";
	return outMessage;
}

function stringRank(rank, group=true) {
	return stringMu(rank.mu, group);
}

function stringMu(mu, group=true){
	display = Math.round(mu*100);
	if(group) return rankGroup(display)+" "+display;
	return ""+display;
}

function stringUpdateRank(before, after, placed){
	//-1 = not placed
	// 0 = JUST placed
	// 1 = been placed a while mate
	if(placed<0) return "Unplaced";
	else if(placed==0){
		return "🎉 Placed: "+stringRank(after)+" 🎉";
	}
	return ""+stringRank(before)+" --> "+stringRank(after);
}

function rankGroup(elo){
	bounds =cfg.rankGroups.values;
	for(let i=0;i<bounds.length;i++){
		if(elo>=bounds[i]) return "["+cfg.rankGroups.icons[i]+"]";
	}
	return "["+cfg.rankGroups.icons[bounds.length]+"]";
}

module.exports = {
	getLeague,
	getRank,
	getLB,
	rankMatch,
	stringUpdateRank,
	rankGroup,
	ranksTable,
	makeRank,
	stringMu,
	stringRank
};
