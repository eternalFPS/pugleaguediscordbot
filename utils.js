const cfg = require('./config.json');

/////////////////
// Communications
// //////////////

//ping = ["everyone", "users", "roles"]
// ^ set what you want to be pinged

function reply(message, out,log="",ping=[]){
	did = grabDid(message);
	msgOpt = {
		content: out,
		allowedMentions: {parse:ping}
	};
	message.reply(msgOpt)
		.then(message => { 
			if(log==="nolog") return;
			else console.log(`${did} Rply: ${log}`);
		})
		.catch(console.error);
}

function send(message, out,log="",ping=[]){
	did = grabDid(message);
	msgOpt = {
		content: out,
		allowedMentions: {parse:ping}
	};
	message.channel.send(msgOpt)
		.then(message => { 
			if(log==="nolog") return;
			else console.log(`${did} Send: ${log}`);
		})
		.catch(console.error);
}

function dm(message, out, log = '') {
	did = grabDid(message);
	message.author.send(out)
		.then((message) => {
			if (log === 'nolog') return;
			else console.log(`${did}   DM: ${log}`);
		})
		.catch(console.error);
}

function grabDid(message){
	if(cfg.shorts.hasOwnProperty(message.guildId)){
		return cfg.shorts[message.guildId];
	}
	return message.guildId.substr(message.guildId.length-3);
}

///////////////////////
// String Getters
// ////////////////////

function cntToEmoji(iso){
	if(iso==null||iso=='') return '?';
	if(!cfg.flags.toEmoji.hasOwnProperty(iso)) return '?';
	return cfg.flags.toEmoji[iso];
}


//fits a string into a set number of characters
function fitString(s, size, align=0){
	s = ''+s;
	if(s.length==size) return s;
	if(s.length>size) return s.substring(0,size);
	if(align<0){
		return s+(" ".repeat(size-s.length));

	}else if(align>0){
		return " ".repeat(size-s.length)+s;
	}else{
		lower = Math.floor((size-s.length)/2);
		higher = size-s.length-lower;
		return " ".repeat(lower)+s+(" ".repeat(higher));
	}
}

function toTimeString(ms){//input as miliseconds (1000ms = 1s)
	sign = '';
	if(ms<0){
		sign = " ago";
		ms=-ms;
	}
	outStr = "";
	secs = Math.floor(ms/1000);
	if(secs>60){
		mins = Math.floor(secs/60);
		secs = secs%60;
		if(mins>60){
			hours = Math.floor(mins/60);
			mins=mins%60;
			if(hours>24){
				days = Math.floor(hours/24);
				hours=hours%24;
				outStr= `${days}d ${hours}h ${mins}m`;
			}else outStr= `${hours}h ${mins}m`;
		}else outStr= `${mins}m`;
	}else outStr= ""+secs+"s";
	return outStr+=sign;
}

function idFromMention(mention){
	if (mention.startsWith('<@') && mention.endsWith('>')) {
		mention = mention.slice(2, -1);
		if (mention.startsWith('!')) {
			mention = mention.slice(1);
		}
	}else mention = '';
	return mention;
}

function idToClass(id){
	return cfg.classsimple[id];
}

///////////////////////////
// Other Utilities
///////////////////////////

//will return -1 if failed
function parseTime(timeStr){
	s = timeStr.split('');
	future = new Date();
	now = Date.now();
	scratch = 0;
	hasLetters=false;
	addSeconds = 0;
	for(let i=0;i<s.length;i++){
		pint = parseInt(s[i],10);
		if(!isNaN(pint)){
			scratch=scratch*10+pint;
		}else{
			hasLetters=true;
			if(scratch==0) scratch=1;
			switch(s[i]){
				case 'Y':
					future.setFullYear(future.getFullYear()+scratch);
					break;
				case 'M':
					future.setMonth(future.getMonth()+scratch);
					break;
				case 'w':
					addSeconds+=604800*scratch;
					break;
				case 'd':
					addSeconds+=86400*scratch;
					break;
				case 'h':
					addSeconds+=3600*scratch;
					break;
				case 'm':
					addSeconds+=60*scratch;
					break;
				case 's':
					addSeconds+=scratch;
					break;
				default:
					return -1;
			}
			scratch=0;
		}
	}
	if(hasLetters) return (1000*addSeconds)+(future.getTime()-now);
	else return -1;
}

function randInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function argParser(args, flags, skip=0){
	/*EXAMPLE OF "flags"
flags = {
	$shorts: function(flag){
		switch(flag){
			case 'p': return "pickups";
			case 'r': return "region";
			case 'g': return "global";
			case 'l': return "leagueid";
			case 's': return "season";
			default: return flag;
		}},
	pickups: 1,
	region: 1,
	global: 0,
	leagueid: 2,
	season: 1
}

	0 = no value required
	1 = value expected
	2 = int expected
	3 = int expected, but return as string (good for guildids/discordids)
	can add more if needed

	//////// R E T U R N   O B J E C T ///////////

	flgVals IS THE RETURN OBJECT
	$err = error message (no error if =="")
	$free = values that were not attached to flags
	"flag name" = value for mentiond flag

	*/
	flgVals = {
		$err: "",
		$free: []
	};
	flgArr = [];
	flgi = 0;
	for(let i=skip;i<args.length;i++){
		if(args[i].charAt(0)=='-'){
			if(args[i].charAt(1)=='-') {
				flag = args[i].slice(2);
				if(flags.hasOwnProperty(flag)) {
					if(flags[flag]==0) flgVals[flag]=true;
					else flgArr.push(flag);
				}else{
					flgVals.$err = "♿ \""+flag+"\" is not a valid flag for that command ♿";
					return flgVals;
				}
			}else{
				for(let j=1;j<args[i].length;j++){
					flag = flags.$shorts(args[i].charAt(j));
					//TODO fix copypasted code, but how would be best to achieve this?
					if(flags.hasOwnProperty(flag)) {
						if(flags[flag]==0) flgVals[flag]=true;
						else flgArr.push(flag);
					}else{
						flgVals.$err = "♿ \""+flag+"\" is not a valid flag for that command ♿";
						return flgVals;
					}
				}
			}
		}else{
			if(flgi>=flgArr.length) flgVals.$free.push(args[i]);
			else{
				flag = flgArr[flgi++];
				switch(flags[flag]){
					case 2: case 3:// INT
						toInt = parseInt(args[i]);
						if(isNaN(toInt)) {
							flgVals.$err = "♿ Expected an integer for \""+flag+"\" flag argument, but instead recieved \""+args[i]+"\" ♿";
							return flgVals;
						}else{
							if(flags[flag]==2) flgVals[flag] = toInt;
							else flgVals[flag] = args[i];
						}
						break;
					case 1: default:
						flgVals[flag] = args[i];
						break;
				}
			}
		}
	}
	if(flgi<flgArr.length) flgVals.$err = "♿ Did not recieve any value for the \""+flgArr[flgi]+"\" flag ♿";
	return flgVals;
}

function guildFromRegion(region, guilds){
	foundregion = null;
	for(var g in guilds){
		if(guilds[g].name == region) {
			foundregion = guilds[g].guildid;
			break;
		}
	}
	if(foundregion==null){
		for(var g in cfg.guilds){
			if(g == region){
				foundregion = cfg.guilds[g];
				break;
			}
		}
	}
	return foundregion;
}

function dateString(date){
	outStr='';
	day = date.getDate();
	if(day<10) outStr+='0';
	outStr+=day;
	months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	outStr+=months[date.getMonth()+1];
	outStr+=date.getFullYear()%100;
	return outStr;

}

module.exports = {
	reply,
	send,
	dm,
	randInt,
	toTimeString,
	parseTime,
	argParser,
	guildFromRegion,
	grabDid,
	fitString,
	dateString,
	idFromMention,
	cntToEmoji,
	idToClass
};

