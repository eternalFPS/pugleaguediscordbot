//they are structures because they can hold a lot if things (information) but they don't do anything (objects/machines). They also are NOT read only (records)

function Guild(r){
	check = ["guildid","overlordid","channelids","leagueid"];
	for(let i=0;i<check.length;i++){
		if(!r.hasOwnProperty(check[i])){
			throw new Error("Tried to create guild without specifying "+check[i]+" variable");
		}
	}
	//references
	this.guildid = r.guildid;
	this.leagueid = r.leagueid;
	this.overlordid = r.overlordid;
	this.name = r.name || "";
	//guildsettings
	this.settings = {
		mainpickupid: r.mainpickupid || null,
		adminrole: r.adminrole || null,
		modrole: r.modrole || null,
		readytime: r.readytime || 30000,
		noaddrole: r.noaddrole || null,
		channelids: r.channelids.split(','),
		matchchannel: r.matchchannel || null,
		promotedelay: r.promotedelay || 1800000,
		promoterole: r.promoterole || null,
		placementgames: r.placementgames || null
	};
	//active variables
	this.active = {};
	this.pickups = {};
	this.lastPromote = 0;
}

function Pickup(r){
	//work before we can initialize
	check = ["name","pickupid","leagueid","guildid"];
	for(let i=0;i<check.length;i++){
		if(!r.hasOwnProperty(check[i])){
			throw new Error("Tried to create pickup without specifying "+check[i]+" variable");
		}
	}
	mappool = [];
	if(r.mappool!=null) mappool = r.mappool.split(',');

	//references
	this.name = r.name;
	this.pickupid = r.pickupid;
	this.guildid = r.guildid;
	this.leagueid = r.leagueid;
	//settings
	this.settings = {
		whiterole: r.whiterole || null, //TODO unused
		blackrole: r.blackrole || null, //TODO unused
		mappool: mappool,
		pickteams: r.pickteams || "auto",
		ranked: r.ranked || 0,
		teamsize: r.teamsize || 4,
		placementgames: r.placementgames || 10
	};
	//Active variables
	this.queue = [];	//stores playerids waiting for a game
	this.matches = {};
	this.ready = {//keeps track of players currently ready
		msgid: null,
		players:null,
		aLength:0
	}
}

function Match(r){
	check = ["matchid","pickupid","state"];
	for(let i=0;i<check.length;i++){
		if(!r.hasOwnProperty(check[i])){
			throw new Error("Tried to create Match without specifying "+check[i]+" variable");
		}
	}
	this.matchid = r.matchid;
	this.pickupid = r.pickupid;
	this.teams = r.teams || [[],[]]; //[[team 1 playerids], [team2 player ids]]
	this.leagueids = r.leagueids || []; //array of leagueids. League to display in index 0.

	/* results:
	 * [d]raw, [c]ancled, [1]team won, [2]team won
	*/
	this.result = r.result || null;
	this.startTime = r.startTime || Date.now();

	this.players = r.players || {};//contains player objects see below

	/* States:
	 * -1 = incomplete match (missing data)
	 * 0 = waiting for ready
	 * 1 = pickteams
	 * 2 = waiting for result
	 *
	 */
	this.state = r.state;
	this.map = r.map || null;
	this.capReport = r.capReport || [null,null];
	/*this.quality = r.quality || 0;
	this.winPercent = r.winPercent || 0;
	this.drawPercent = r.drawPercent || 0;*/
	this.teamSum = r.teamSum || [0,0];
	this.pickteams = r.pickteams || null;
	//the following are for manual pick teams only
	this.pickorder = r.pickorder || null;// e.g. [0,1,1,0]
	this.scratch = r.scratch || null; //orderd list of unpicked players
	this.teamind = r.teamind || null; //[0,0]
}

function Player(r={}){
	this.nickname = r.nickname || null;
	this.leagues = r.leagues || {}; //see comment below
	this.matchId = r.matchId || null;
	this.punished = r.punished || 0;
	this.queuedAt = r.queuedAt || Date.now();
	this.idleSince = r.idleSince || null;
	this.premium = r.premium || 0;
}
// TODO decide if leagues will be deleted from players. Punished also
// Maybe with punished, it gets set to the date in which their ban expires, and every minute or so, we just go through all active players and see if their ban expired, then lift their role.

/* The player.leagues consists of the following
 * key: leagueid
 *  - rank = opensill rank
 *  - maxrank
 *  - wins
 *  - draws
 *  - losses
 *  - streak
 */ 

module.exports = {
	Player,
	Pickup,
	Match,
	Guild
};
