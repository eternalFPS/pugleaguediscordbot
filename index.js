const { Client, Intents } = require('discord.js');
const cfg = require('./config.json');
const { Guild, Pickup} = require('./structs.js');
const u = require('./utils.js');
const d = require('./dbreqs.js');
const mm = require('./matchmaker.js');
const a = require('./admin.js');
const rs = require("./ranksys.js");

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

// Create a new client instance
const client = new Client({ 
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES,
		Intents.FLAGS.GUILD_MESSAGE_REACTIONS
	] });

const prefix = "!";
var stats = {};
var guilds = {};
var activeChannels = [];

// When the client is ready, run this code (only once)
client.once('ready', () => {
	d.initStats(function(row) {
		stats = row;
		console.log(stats);
	});
	d.initGuilds(function(r){// the r here is a row from the guilds table
		guilds[r.guildid] = new Guild(r);
		activeChannels = activeChannels.concat(r.channelids.split(','));
	}, function(r) {//the r here is a row from the PICKUPS table
		guilds[r.guildid].pickups[r.pickupid] = new Pickup(r);
	}, function(r){
		console.log("   ###   Online!   ###");
		for(var gid in guilds){
			console.log(""+guilds[gid].name+": "+gid);
			for(var p in guilds[gid].pickups){
				console.log("   "+guilds[gid].pickups[p].name+": "+p);
			}
		}
	});
});

var timers = [];//array of timer objects, ordered by newest expiring first
/*timer = {
 *	time: (datetime when this timer is to trigger)
 *	exe: (function name to execute (a string of the func name))
 *	input: input(s) (depending on how you want to execute it
 */

client.on("messageCreate", message => {
	try{
		while(timers.length>0 && Date.now()>timers[0].time){//cheap trick for timed events
			switch(timers[0].exe){
				case "mm.readyFail":
					/* Expected input:
					 * guildid
					 * pickupid
					 */
					discordjsfuckfight = client.guilds.cache.get(timers[0].input.guildid).channels.cache.get(guilds[timers[0].input.guildid].settings.channelids[0]); //the power of code!
					mm.readyFail(
						guilds[timers[0].input.guildid],
						timers[0].input.pickupid,
						discordjsfuckfight
					);
					break;
				default:
					return;
			}
			timers.shift();
		}
		if (message.author.bot) return;
		if (!(activeChannels.includes(message.channelId))) {
			if(message.content === "!pickups_enable"){
				if(guilds.hasOwnProperty(message.guildId)) return;
				//mguildId to escape naming issues
				mguildId = message.guildId;
				overlord = message.author.id;
				channelId = message.channelId.toString();
				leagueid = stats.nextleagueid++;

				r = {
					guildid: mguildId,
					overlordid: overlord,
					channelids: channelId,
					leagueid: leagueid
				};
				guilds[mguildId] = new Guild(r);
				activeChannels.push(channelId);
				d.createGuild(mguildId,overlord,channelId,leagueid);
				d.updateStats(stats);
				u.reply(message, "Pickups enabled on this channel (but you still need to set them up)", "Enabled pickups");
			}
			return;
		}
		const guildId = message.guildId;
		if (!message.content.startsWith(prefix)){
			//Fast commands (no prefix)
			argz = message.content.split(' '); //WITH A Z on argz
			switch(argz[0]){
				case "++":
					mm.addQueue(guilds,message, argz, timers);
					break;
				case "--":
					mm.removeQueue(guilds[guildId],message, message.author.id, argz);
					break;
				default:
					break;
			}
			return;
		}
		const commandBody = message.content.slice(prefix.length);
		const args = commandBody.split(' ');
		const command = args[0].toLowerCase();

		switch(command){
			case "createpickup": case "addpickup":
				if(!a.hasPerms(message,guilds[guildId],100)){
					return u.reply(message, "Only admins can make new pickups");
				}
				if(args.length<2) return u.reply(message, "its !createpickup <name>");
				pickupid = stats.nextpickupid++;
				leagueid = stats.nextleagueid++;
				name = args[1];
				pickupinfo = {
					name: name,
					pickupid: pickupid,
					leagueid: leagueid,
					guildid: guildId
				};
				guilds[guildId].pickups[pickupid] = new Pickup(pickupinfo);
				mainpickup = false;
				if(guilds[guildId].settings.mainpickupid==null) {
					guilds[guildId].settings.mainpickupid = pickupid;
					mainpickup = true;
				}
				d.createPickup(pickupinfo, mainpickup);
				d.updateStats(stats);
				u.reply(message,`Pickup ${name} created with id:${pickupid}`);
				break;
			case "test":
				mm.requireReady(message);
				break;
			case "ping":
				u.reply(message,`latency: ${Date.now()-message.createdTimestamp}ms.`);
				break;
			case "commands": case "help":
				u.reply(message, "https://gitlab.com/eternalFPS/pugleaguediscordbot/-/blob/master/commands.md");
				break;
			case "add":case "q":
				mm.addQueue(guilds,message, args, timers);
				break;
			case "remove":
				mm.removeQueue(guilds[guildId],message, message.author.id, args);
				break;
			case "who":
				u.reply(message, mm.stringQueues(guilds[guildId]), "Printed Queues");
				break;
			case "matches":
				mm.printMatches(guilds[guildId],message);
				break;
			case "match":case "teams":case "map":
				mm.printMatch(guilds[guildId],message,args);
				break;
			case "rl":
				mm.report(guilds[guildId], message, 'l'); // lucy in the
				break;
			case "rc":
				mm.report(guilds[guildId], message, 'c'); // cky with
				break;
			case "rd":
				mm.report(guilds[guildId], message, 'd'); // diamonds
				break;
			case "set":
				a.set(message, guilds[guildId], args);
				break;
			case "maps": case "mappool":
				a.printMaps(message, args, guilds);
				break;
			case "noadd": case "ban":
				a.noadd(message, args, guilds[guildId]);
				break;
			case "rank": case "player":
				rs.getRank(message,args, guilds);
				break;
			case "lb": case "leaderboard":
				rs.getLB(message, args, guilds);
				break;
			case "noadds": case "bans":
				a.listNoadds(message, guilds, args);
				break;
			case "cfg": case "config":
				a.printConfig(message, args, guilds);
				break;
			case "stats":
				a.printStats(message, args, guilds);
				break;
			case "lastgame": case "lastmatch":
				mm.lastGame(message, args, guildId, guilds);
				break;
			case "top":
				a.top(message, args, guilds, guildId);
				break;
			case "promote":
				a.promote(message, guilds[guildId]);
				break;
			case "forgive":
				a.forgive(message, args, guilds[guildId]);
				break;
			case "ranks": case "rankstable": case "ranks_table":
				rs.ranksTable(message, args, guilds, guildId);
				break;
			case "pick":
				mm.pick(message, args, guilds[guildId]);
				break;
			default:
				d.recCmd(commandBody);
				return;
		}
	} catch(e){
		console.log("!!! ERROR !!!");
		console.log(e);
	}

});

client.on("messageReactionAdd", function(msgRea, usr) {
	reactor(msgRea, usr, true);
});

client.on("messageReactionRemove", function(msgRea, usr) {
	reactor(msgRea, usr, false);
});


function reactor(msgRea, usr, pushedIn=false){
	try{
		if (usr.bot) return;
		message = msgRea.message;
		if (!(activeChannels.includes(message.channelId))) return;
		action = false;
		switch(msgRea.emoji.name){
			case '✅': 
				action = pushedIn;
				break;
			case '⛔':
				if(!pushedIn) return;
				action = false;
				break;
			default: return;
		}
		guildId = message.guildId;
		if(!guilds.hasOwnProperty(guildId)) {
			throw new Error("Active channel but no active Guild");
			return;
		}
		puID = -1;
		for(var pck in guilds[guildId].pickups){
			if(guilds[guildId].pickups[pck].ready.msgid = message.id){
				puID = pck;
				break;
			}
		}
		if(puID==-1) return;
		pickup = guilds[guildId].pickups[puID];
		if(action){
			for(let i=0;i<pickup.ready.aLength;i++){
				if(pickup.ready.players[i]==usr.id) return;
			}
			qPos = -1;
			for(let i=0;i<pickup.queue.length;i++){
				if(pickup.queue[i]==usr.id){
					qPos = i;
					break;
				}
			}
			if(qPos==-1) return;
			pickup.ready.players[pickup.ready.aLength++]=usr.id;
			if(pickup.ready.aLength>=pickup.settings.teamsize*2){
				m = mm.createMatch(guilds,pickup,stats.nextmatchid);
				pickup.matches[stats.nextmatchid] = m;
				mm.embedMatch(message,m,true);
				d.incMatchid(++stats.nextmatchid);
				console.log(timers);
				if(timers.length==0) return;
				nTimers = new Array(timers.length-1);
				ni = 0;
				for(let i=0;i<timers.length;i++){
					if(timers[i].exe=="mm.readyFail") continue;
					if(ni>nTimers.length) {
						throw new Error("Timer Already Removed???");
						return;
					}
					nTimers[ni++]=timers[i];
				}
				timers = nTimers;
			}
		}else{
			removed = false;
			for(let i=0;i<pickup.ready.aLength;i++){
				if(pickup.ready.players[i]==usr.id){
					for(let x=i;x<pickup.ready.aLength-1;x++){
						pickup.ready.players[x] = pickup.ready.players[x+1];
					}
					pickup.ready.aLength--;
					removed = true;
					break;
				}
			}
			if(!removed) return;
		}
		rdyMsg = "**Match Ready**, BUT ARE YOU???\n";
		for(let i=0;i<pickup.queue.length;i++){
			heReady = false;
			for(let x=0;x<pickup.ready.aLength;x++){ //inefficient?
				if(pickup.queue[i]==pickup.ready.players[x]){
					heReady = true;
					break;
				}
			}
			if(!heReady) rdyMsg+="<@"+pickup.queue[i]+"> ";
		}
		rdyMsg+="\nReact with ✅ to mark yourself ready to play!";
		message.edit({
			content: rdyMsg,
			cllowedMentions: {parse:["users"]}
		});
	}catch(e){
		console.log("!!! ERROR !!!");
		console.log(e);
	}
}

//directly type commands into console, good for testing/debugging
readline.on('line', (line) => {
	args=line.split(' ');
	switch(args[0]){
		case "stats":console.log(stats);break;
		case "guilds":console.log(Object.keys(guilds));break;
		case "activeChannels":case "activechannels":case "activechans":
			console.log(activeChannels);break;
		case "guild":
			if(args.length<2) console.log("need to specify which guild");
			else if(guilds.hasOwnProperty(args[1])){
				console.log(guilds[args[1]]);
			}else console.log("not a valid guild");
			break;
		case "pickups":
			for(var gid in guilds){
				console.log(""+guilds[gid].name+": "+gid);
				for(var p in guilds[gid].pickups){
					console.log("   "+guilds[gid].pickups[p].name+": "+p);
				}
			}
			break;
		case "active":
			for(var gid in guilds){
				console.log(""+guilds[gid].name+": "+gid);
				console.log(guilds[gid].active);
			}
			break;
		case "tree":
			for(var gid in guilds){
				console.log(""+guilds[gid].name+": "+gid);
				for(var p in guilds[gid].pickups){
					console.log("  "+guilds[gid].pickups[p].name+": "+p);
					console.log("    In Queue: "+guilds[gid].pickups[p].queue.length);
					console.log("    Matches:");
					matches = guilds[gid].pickups[p].matches;
					for(var m in matches){
						console.log("      "+m+": "+matches[m].state);
					}
				}
			}
			break;
		case "matchplayers":
			if(args.length<2) console.log("need to specify match");
			else{
				for(var gid in guilds){
					for(var p in guilds[gid].pickups){
						if(guilds[gid].pickups[p].matches.hasOwnProperty(args[1])){
							outStr="";
							matPly = guilds[gid].pickups[p].matches[args[1]].players;
							for(var k in matPly){
								player =matPly[k];
								outStr+=player.nickname+"\n";
								for(var l in player.leagues){
									outStr+=player.leagues[l].rank.mu;
									outStr+="\n";
								}
							}
							console.log(outStr);
							return;
						}
					}
				}
			}
			break;
		default:console.log("console command not recognised");break;
	}
});

readline.on("close", () => {
	d.close((e) => {
		if(!e) {
			client.destroy();//due to non-sequential order of js, im gonna say that this never actually gets ran
			process.exit(0);
		}
		else{
			console.log(e);
			client.destroy();
			process.exit(1);
		}
	});
});


// Login to Discord with your client's token
client.login(cfg.token);

